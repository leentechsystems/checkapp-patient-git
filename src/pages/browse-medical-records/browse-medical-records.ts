import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { ProfileMedicalRecordsPage } from './../profile-medical-records/profile-medical-records';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the BrowseMedicalRecordsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse-medical-records',
  templateUrl: 'browse-medical-records.html',
})
export class BrowseMedicalRecordsPage {
 
  public records_tabs = "consultations";
  all_consultations: any;
  all_procedures: any;
  all_laboratory: any;
  current_consultation_page = 1;
  current_procedure_page = 1;
  current_laboratory_page = 1;
  total_consultation_count = 0;
  total_procedures_count = 0;
  total_laboratory_count = 0;
  user_profile: any;
  from_notif = (this.navParams.get('from_notif')) ? this.navParams.get('from_notif') : null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider, 
    public storage: Storage, public toastCtrl: ToastController, public viewCtrl: ViewController) {

    this.loadAllData(false, true, false); 
    this.storage.get("sess_user_login").then((user) => {
        this.user_profile = user;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BrowseMedicalRecordsPage');
  }

  gotoProfileMedicalRecord(thisReport) {
  	this.navCtrl.push(ProfileMedicalRecordsPage, {
        "report": thisReport
    });
  }

  loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
      if(refresher) { /* if refreshed */
          this.current_laboratory_page = 1; /* reset the current page to default */
          this.current_procedure_page = 1; /* reset the current page to default */
          this.current_consultation_page = 1; /* reset the current page to default */

          this.all_consultations = null;
          this.all_procedures = null;
          this.all_laboratory = null;
      }

      /* - load functions responsible on getting and processing the data 
      - we'll pass refresher so we will stop the refresher event after processing the data NOT promptly
      - we'll pass infiniteScroll so we will stop the infiniteScroll event after processing the data NOT promptly */

      this.loadAllConsultation(refresher, isLoadNew, infiniteScroll); 
      this.loadAllProcedure(refresher, isLoadNew, infiniteScroll); 
      this.loadAllLaboratory(refresher, isLoadNew, infiniteScroll); 
  }

  loadAllConsultation(refresher?, isLoadNew?, infiniteScroll?) {
          /* getting of data from the provider, we'll passing set of data the function is requiring */
          let thisData = {
            "current_page": this.current_consultation_page, /* separate each parameters bty comma*/
            "type": "consultation", 
          };

          this.userProvider.getMedicalReport(isLoadNew, thisData).then(data => {
            if(data['error'] === 0) {
              this.total_consultation_count = data['result']['total_count'];

              if(!infiniteScroll && infiniteScroll === false) {
                   /* converting into array if variable is empty */
                  /* emptying the variable when refreshed */
                  if(!this.all_consultations || isLoadNew === true) {
                     this.all_consultations = []; 
                  }
              }

              data['result']['items'].forEach((cat, idx) => {
                    if(cat.id !== undefined) {
                        this.all_consultations.push(cat);
                    }
              });

              if(infiniteScroll) {
                  infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
              }
            } 

            if(refresher) {
              refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
            }
        });
  } 

  loadAllProcedure(refresher?, isLoadNew?, infiniteScroll?) {
       /* getting of data from the provider, we'll passing set of data the function is requiring */
          let thisData = {
            "current_page": this.current_procedure_page, /* separate each parameters bty comma*/
            "type": "procedure", 
          };

          this.userProvider.getMedicalReport(isLoadNew, thisData).then(data => {
            if(data['error'] === 0) {
              this.total_procedures_count = data['result']['total_count'];

              if(!infiniteScroll && infiniteScroll === false) {
                   /* converting into array if variable is empty */
                  /* emptying the variable when refreshed */
                  if(!this.all_procedures || isLoadNew === true) {
                     this.all_procedures = []; 
                  }
              }

              data['result']['items'].forEach((cat, idx) => {
                    if(cat.id !== undefined) {
                        this.all_procedures.push(cat);
                    }
              });

              if(infiniteScroll) {
                  infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
              }
            }

            if(refresher) {
              refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
            }
        });
  } 

  loadAllLaboratory(refresher?, isLoadNew?, infiniteScroll?) {
       /* getting of data from the provider, we'll passing set of data the function is requiring */
          let thisData = {
            "current_page": this.current_laboratory_page, /* separate each parameters bty comma*/
            "type": "laboratory", 
          };

          this.userProvider.getMedicalReport(isLoadNew, thisData).then(data => {
            if(data['error'] == 0) {
                this.total_laboratory_count = data['result']['total_count'];

                if(!infiniteScroll && infiniteScroll === false) {
                     /* converting into array if variable is empty */
                    /* emptying the variable when refreshed */
                    if(!this.all_laboratory || isLoadNew === true) {
                       this.all_laboratory = []; 
                    }
                }

                data['result']['items'].forEach((cat, idx) => {
                      if(cat.id !== undefined) {
                          this.all_laboratory.push(cat);
                      }
                });
                
                if(infiniteScroll) {
                    infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
                }
            }

            if(refresher) {
              refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
            }
        });
  } 

   doInfinite(infiniteScroll:any, thisType) {

      if(thisType === "laboratory") {
           this.current_laboratory_page += 1;
           if(this.all_laboratory.length < this.total_laboratory_count) {
              this.loadAllLaboratory(false, false, infiniteScroll);
           } else {
              infiniteScroll.enable(false);
           }
      } else if(thisType === "procedure") { 
          this.current_procedure_page += 1;
           if(this.all_procedures.length < this.total_procedures_count) {
              this.loadAllProcedure(false, false, infiniteScroll);
           } else {
              infiniteScroll.enable(false);
           }
      } else {
           this.current_consultation_page += 1;
           if(this.all_consultations.length < this.total_consultation_count) {
              this.loadAllConsultation(false, false, infiniteScroll);
           } else {
              infiniteScroll.enable(false);
           }
      }
  }

  gobacktoNotif(){
    this.viewCtrl.dismiss();
  }

  presentToast(msg) {
    this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: "top"
    }).present();
  }
}
