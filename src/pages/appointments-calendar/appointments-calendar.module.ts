import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentsCalendarPage } from './appointments-calendar';

@NgModule({
  declarations: [
    AppointmentsCalendarPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentsCalendarPage),
  ],
})
export class AppointmentsCalendarPageModule {}
