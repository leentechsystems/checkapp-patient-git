import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { ProfileAppointmentPage } from '../../pages/profile-appointment/profile-appointment';
/**
 * Generated class for the AppointmentsCalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-appointments-calendar',
	templateUrl: 'appointments-calendar.html',
})
export class AppointmentsCalendarPage {
	date: any;
	daysInThisMonth: any;
	daysInLastMonth: any;
	daysInNextMonth: any;
	monthNames: string[];
	currentMonth: any;
	currentMonthIdx: any;
	currentYear: any;
	currentDate: any;
	date_selected: any;

	overall_results: any;
	all_results: any;
	all_upcoming: any;
	all_previous: any;
	current_page = 1; 
	total_count = 0;
	today: any;
	this_year: any;
	upcoming_list = [];
	from_notif = (this.navParams.get('from_notif')) ? this.navParams.get('from_notif') : null;

	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				private calendar: Calendar, 
				public appointmentProvider: AppointmentProvider, 
				public modalCtrl: ModalController,
				public viewCtrl: ViewController) {

		this.today = new Date();
		this.this_year = this.today.getFullYear();

		if(this.from_notif){
			this.date = new Date();
			this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
			
			this.all_results = null;
			this.all_upcoming = null;
			this.loadAllData(false, false, false); 
			this.getDaysOfMonth();
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad AppointmentsCalendarPage');
	}

	ionViewWillEnter() {
	}

	ionViewDidEnter() {
		if(!this.from_notif){
			this.date = new Date();
			this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
			
			this.all_results = null;
			this.all_upcoming = null;
			this.loadAllData(false, false, false); 
			this.getDaysOfMonth();
		}
	}

	getDaysOfMonth() {
		this.daysInThisMonth = new Array();
		this.daysInLastMonth = new Array();
		this.daysInNextMonth = new Array();
		this.currentMonth = this.monthNames[this.date.getMonth()];
		this.currentMonthIdx = this.date.getMonth();
		this.currentYear = this.date.getFullYear();

		if(this.date.getMonth() === new Date().getMonth()) {
			this.currentDate = new Date().getDate();
		} else {
			this.currentDate = 999;
		}

		var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
		var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
	 
		for(var i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
			this.daysInLastMonth.push(i);
		}
		var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
		
		for (var j = 0; j < thisNumOfDays; j++) {
			let day_data = {
				"day": j+1
			}
			this.daysInThisMonth.push(day_data);
		}

		var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
		// var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
		for (var k = 0; k < (6-lastDayThisMonth); k++) {
			this.daysInNextMonth.push(k+1);
		}

		var totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
		if(totalDays<36) {
			for(var l = (7-lastDayThisMonth); l < ((7-lastDayThisMonth)+7); l++) {
				this.daysInNextMonth.push(l);
			}
		}
	}

	goToLastMonth() {
		this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
		this.getDaysOfMonth();
	}

	goToNextMonth() {
		this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
		this.getDaysOfMonth();
	}

	loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.current_page, /* separate each parameters bty comma*/
			"date": "all",
			"orderdirection": "desc"
		};

		this.appointmentProvider.getPatientAppointments(thisData).then(data => {
			console.log(data);
			if(data['error'] === 0) {
				this.total_count = parseInt(data['appointments']['total_count']);

				if(!infiniteScroll && infiniteScroll === false) {
					 /* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.all_results || isLoadNew === true) {
						 this.all_results = []; 
					}
					if(!this.all_upcoming || isLoadNew === true) {
						 this.all_upcoming = []; 
					}
					if(!this.overall_results || isLoadNew === true) {
						 this.overall_results = []; 
					}
				}

				data['appointments']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						this.overall_results.push(cat);
						let pieces = cat.appointment_date.split(" "); 
						let wordCount = pieces[0].split("-");
						let timeCount = pieces[1].split(":");
						let date_target_year = wordCount[0];
						let date_target_month = wordCount[1];
						let date_target_day = wordCount[2];
						let date_target_hour = timeCount[0];
						let date_target_minutes = timeCount[1];

						let mlist = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
						let convert_appointment_date = new Date(date_target_year, parseInt(date_target_month)-1, date_target_day, date_target_hour, date_target_minutes);
						
						/* Convertion to 12-hour time */
						let new_selected_hour = "";
						let new_selected_time_unit = "";
						if(parseInt(date_target_hour) > 12) {
							new_selected_time_unit = "PM";
							let new_selected_hour1 = parseInt(date_target_hour) - 12;
							if(new_selected_hour1 < 10) {
								new_selected_hour = "0" + new_selected_hour1.toString();
							} else {
								new_selected_hour = new_selected_hour1.toString();
							}
						} else {
							new_selected_time_unit = "AM";
							new_selected_hour = date_target_hour;
						}
						
						cat['appointment_date_day'] =  date_target_day;
						cat['appointment_date_month'] =  mlist[date_target_month - 1];
						cat['appointment_date_year'] =  parseInt(date_target_year);
						cat['appointment_date_time'] =  new_selected_hour +":"+ date_target_minutes + "" + new_selected_time_unit;

						if(cat['doctor_user_profile_attachments_filename'] && cat['doctor_user_profile_attachments_filepath']){
							cat['doctor_attachment'] = {
								filename : cat['doctor_user_profile_attachments_filename'],
								filepath : cat['doctor_user_profile_attachments_filepath']
							}
						}

						if(convert_appointment_date < this.today) {
							/* previous */
							this.all_results.push(cat);
						} else {
							this.all_upcoming.push(cat);
							this.upcoming_list.push(date_target_year+"-"+date_target_month+"-"+date_target_day);
						}
					}
				});

				if(infiniteScroll) {
					infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
				}
			}
				
			if(refresher) {
				refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
			}
		});
	}

	doInfinite(infiniteScroll:any) {
		this.current_page += 1;
		if(this.overall_results.length < this.total_count) {
			this.loadAllData(false, false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
	}

	reloadData(refresher?){
		this.all_results = null;
		this.upcoming_list = [];
		this.current_page = 1;
		this.loadAllData(refresher, true, false);
	}

	checkIfAppointment(day, month, year) {
		month = parseInt(month) + 1;

		if(month < 10) {
			month = "0" + month.toString();
		}

		if(day < 10) {
			day = "0" + day.toString();
		}

		let assemble_date = year + "-" + month + "-" + day;
		if(this.upcoming_list.indexOf(assemble_date) > -1) {
			return "yes";
		} else {
		 	return "none";
		}
	}

	selectDate(thisDay) {
		console.log(thisDay);
		this.date_selected = thisDay;
	}

	goToDetails(thisDetails) {
		let modal = this.modalCtrl.create(ProfileAppointmentPage, {
				"appointment_details": thisDetails
		});
		modal.present();
	}    

	gobacktoNotif(){
		this.viewCtrl.dismiss();
	}
}


