import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HmoProvider } from '../../providers/hmo/hmo';
import { HmoAccountBillDetailsPage } from '../../pages/hmo-account-bill-details/hmo-account-bill-details';

/**
 * Generated class for the PaymentHistoryModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-payment-history-modal',
	templateUrl: 'payment-history-modal.html',
})
export class PaymentHistoryModalPage {

	hmo_account = this.navParams.get("hmo_account");
	payment_history: any;
	current_page = 1;
	total_payment_history = 0;

	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				public viewCtrl: ViewController,
				public hmoProvider: HmoProvider) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad PaymentHistoryModalPage');
	}

	ionViewDidEnter() {
		this.loadAllData(true, false);
	}

	loadAllData(isLoadNew?, infiniteScroll?) {
		if(isLoadNew = true) { /* if refreshed */
			this.current_page = 1; /* reset the current page to default */
		}

		/* - load functions responsible on getting and processing the data
		- we'll pass refresher so we will stop the refresher event after processing the data NOT promptly
		- we'll pass infiniteScroll so we will stop the infiniteScroll event after processing the data NOT promptly */

		this.loadAllPaymentTransaction(isLoadNew, infiniteScroll);
	}

	loadAllPaymentTransaction(isLoadNew?, infiniteScroll?){

		 /* getting of data from the provider, we'll passing set of data the function is requiring */
				let thisData = {
					"hmo_account_id": this.hmo_account.id,
					 "current_page": this.current_page, /* separate each parameters bty comma*/
				};

		this.hmoProvider.getPaymentHistoryTransactions(isLoadNew, thisData).then(data => {
			if(data['error'] == 0) {
				this.total_payment_history = data['payment_history']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					/* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.payment_history || isLoadNew === true) {
						this.payment_history = [];
					}
				}

				data['payment_history']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						this.payment_history.push(cat);
					}
				});

				if(infiniteScroll) {
					infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
				}
			}
		});
	}

	doInfinite(infiniteScroll:any) {
		this.current_page += 1;
		if(this.payment_history.length < this.total_payment_history) {
			this.loadAllPaymentTransaction(false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
	}

	gotoPaymentDetails(paymentDetails) {
		this.navCtrl.push(HmoAccountBillDetailsPage, {
			"hmo_account": paymentDetails,
			"pageType": 'payment_details'
		});
	}

	goBack() {
		this.viewCtrl.dismiss();
	}

	stopPropagation(e) {
	  e.stopPropagation();
	}
}
