import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentHistoryModalPage } from './payment-history-modal';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    PaymentHistoryModalPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(PaymentHistoryModalPage),
  ],
})
export class PaymentHistoryModalPageModule {}
