import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormAppointmentConsultationDetailsPage } from './form-appointment-consultation-details';

@NgModule({
  declarations: [
    FormAppointmentConsultationDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(FormAppointmentConsultationDetailsPage),
  ],
})
export class FormAppointmentConsultationDetailsPageModule {}
