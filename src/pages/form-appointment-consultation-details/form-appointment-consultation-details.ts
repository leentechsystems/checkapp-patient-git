import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, ActionSheetController } from 'ionic-angular';
import { FormAppointmentSchedulePage } from '../../pages/form-appointment-schedule/form-appointment-schedule';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera } from '@ionic-native/camera';
import { normalizeURL } from 'ionic-angular';

/**
 * Generated class for the FormAppointmentConsultationDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  	selector: 'page-form-appointment-consultation-details',
  	templateUrl: 'form-appointment-consultation-details.html',
})
export class FormAppointmentConsultationDetailsPage {
  	public consultationForm: FormGroup;
  	is_submit = false;
  	doctor_profile = this.navParams.get("doctorProfile");
  	consultation_type = this.navParams.get("consultation_type") ? this.navParams.get("consultation_type") : null;
  	photos: any;
  	today = new Date;
  	hospital_service_id: any;

  	constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, 
				public toastCtrl: ToastController, public loadingCtrl: LoadingController, public actionSheetCtrl: ActionSheetController,
				public imagePicker: ImagePicker, private camera: Camera) {

		this.consultationForm = formBuilder.group({
			doctor_id: ['', Validators.required],
			description: ['', Validators.required],
			temperature: [''],
			height: [''],
			weight: [''],
			blood_pressure: [''],
			transaction_type: ['', Validators.required],
			hospital_doctors_hospital_id: ['', Validators.required],
		});
  	}

  	ionViewDidLoad() {
		console.log('ionViewDidLoad FormAppointmentConsultationDetailsPage');
  	}

  	goToAppointmentFormStep2() {
		this.is_submit = true;
		console.log(this.consultationForm);
		console.log(this.hospital_service_id);
	 	if(this.consultationForm.valid && this.hospital_service_id) {
    		this.consultationForm.value['hospital_service_id'] = this.hospital_service_id;
			this.navCtrl.push(FormAppointmentSchedulePage, {
				'doctorProfile': this.doctor_profile,
				'consultationForm': this.consultationForm.value,
				'photos': this.photos
			});
	 	} else {
			this.presentToast("please fill-up necessary fields");
	 	}
  	}

  	presentToast(msg) {
		this.toastCtrl.create({
			message: msg,
			duration: 2000,
			position: "top"
		}).present();
  	}

  	presentActionSheet() {
	  	let actionSheet = this.actionSheetCtrl.create({
			title: 'Select Image Source',
			buttons: [{
			  	text: 'Load from Library',
			  	handler: () => { this.openImagePicker(); }
		  	},
		  	{
		  	    text: 'Use Camera',
		  	    handler: () => { this.takePicture(); }
		  	},
		  	{
				text: 'Cancel',
				role: 'cancel'
			}]
	  	});
	  	actionSheet.present();
  	}

   	openImagePicker(){
	  	if(!this.photos) {
			this.photos = new Array<string>();
	  	}
	  	let maxImagesCtr = 3;

	  	if(this.photos.length >= 3) { 
			maxImagesCtr = 0;
	  	} else {
			maxImagesCtr = 3 - this.photos.length;
	  	}

	  	if(this.photos.length < 3) {
		  	let options= {
				maximumImagesCount: maxImagesCtr,
				quality: 70,
		  	}

		  	this.imagePicker.getPictures(options).then((results) => {
			  	if(results !== 'OK'){
				  	results.forEach((pp, pidx) => {
						let pp_pieces = pp.split("/");
						let new_pp = {
					  		"name": pp_pieces[pp_pieces.length - 1],
					  		"path": normalizeURL(pp)
						};
					this.photos.push(new_pp);
				  	});
				}
		  	}, (err) => { console.log(err) });
	 	} else {
			this.presentToast("You can upload upto three (3) photos only");
	 	}
  	}

  	takePicture(){
	  	if(!this.photos) {
			this.photos = new Array<string>();
	  	}

	  	if(this.photos.length < 3) {
		  	let options = {
				quality: 70,
				correctOrientation: true
		  	};

		  	this.camera.getPicture(options).then((data) => {
			  	let pp_pieces = data.split("/");
			  	let new_pp = {
					"name": pp_pieces[pp_pieces.length - 1],
					"path": normalizeURL(data)
			  	};
			  	this.photos.push(new_pp);
		  	}, function(error) {
			 	console.log(error);
		  	});
	  	} else {
			this.presentToast("You can upload upto three (3) photos only");
	  	}
  	}

  	removePhoto(thisIdx) {
	  	this.photos.splice(thisIdx, 1);
  	}

}
