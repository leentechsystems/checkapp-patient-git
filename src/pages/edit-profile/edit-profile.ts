import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';
import { SiteProvider } from '../../providers/site/site';
import { ModalInfoPage } from '../../pages/modal-info/modal-info'
import { IonicSelectableComponent } from 'ionic-selectable';

/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  	selector: 'page-edit-profile',
  	templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
  	public profileForm: FormGroup;
  	suffixes: any;
  	user_profile = this.navParams.get('user_profile');
  	hmo_account= this.navParams.get('hmo_account');
  	contact = this.navParams.get('contact');
  	editing_section = this.navParams.get('editing_section');
  	is_submit = false;

    gender: any;
  	civil_status = "single";
  	regions: any;
  	cities: any;
  	zipcode: any;
  	total_zip_code = 0;
  	is_change_region = false;
  	is_change_city = false;
  	zip: any;
  	city : any;
  	region: any;

  	constructor(public navCtrl: NavController,
  		public navParams: NavParams,
  		public formBuilder: FormBuilder,
		public toastCtrl: ToastController,
		public userProvider: UserProvider,
		public alertCtrl: AlertController,
		public storage: Storage,
		public loadingCtrl: LoadingController,
		public siteProvider: SiteProvider,
		public modalCtrl: ModalController) {
	  	console.log("editing_section", this.editing_section);
	  	console.log("user_profile", this.user_profile);
	  	console.log("hmo", this.hmo_account);
        console.log("user_profile",this.user_profile);

	  	this.storage.get("sess_regions").then(regions => {regions
		  	this.regions = regions;
            console.log(regions);
		  	if(this.user_profile['address_region'] && this.user_profile['address_region']['provDesc']){
				this.getCity(this.user_profile['address_region']['provDesc']);
		  	} else if(this.user_profile['address_region']){
                this.getCity(this.user_profile['address_region']);
            }
		  	if(this.user_profile['address_zip']){
				this.getZipcode();
		  	}
	  	});

        if(this.user_profile){
            this.city = this.user_profile['address_city'];
            this.region = this.user_profile['address_region'];
            this.zip = this.user_profile['address_zip'];
        }
        console.log(this.user_profile['address_region']);

		this.profileForm = formBuilder.group({
		  	email: [this.user_profile['email'], Validators.required],
		  	firstname: [this.user_profile['firstname'], Validators.required],
		  	lastname: [this.user_profile['lastname'], Validators.required],
		 	midlename: [this.user_profile['midlename']],
		  	mothers_maiden_name: [this.user_profile['mothers_maiden_name']],
		  	date_of_birth: [this.user_profile['date_of_birth'], Validators.required],
		  	place_of_birth: [this.user_profile['place_of_birth'], Validators.required],
		  	height: [this.user_profile['height']],
		  	weight: [this.user_profile['weight']],
		  	company_name: [this.hmo_account['company_name']],
		  	designation: [this.hmo_account['company_job_title']],
		  	tin_no: [this.hmo_account['tin_no']],
		  	civil_status: [this.user_profile['civil_status']],
		  	gender: [this.user_profile['gender']],
		  	biography: [this.user_profile['biography']],
			country: ['PH'],
		  	region: [this.region],
		  	city: [this.city],
		  	address: [this.user_profile['address_address'], Validators.required],
		  	zip: [this.zip],
		  	suffix: [this.user_profile['suffix']],
		  	address_id: [this.user_profile['address_id']],
		  	id: [this.user_profile['id'], Validators.required],
		  	status: [this.user_profile['status'], Validators.required],
		  	user_role_id: [this.user_profile['user_role_id'], Validators.required],
	  	});
  	}

  	ionViewDidLoad() {
		console.log('ionViewDidLoad EditProfilePage');
        console.log('CITY: ',this.city);
        console.log('REGION: ',this.region);
  	}

    regionChange(event: {
        component: IonicSelectableComponent,
        value: any
    }) {
        this.region = event.value['provDesc'] ? event.value['provDesc'] : event.value;
        this.city = '';
        this.getCity(this.region);
        console.log('Region Value:', event.value);
        console.log('Region', this.region);
      }

      cityChange(event: {
          component: IonicSelectableComponent,
          value: any
      }) {
          this.getZipcode();
        }

  	presentToast(msg) {
	  	this.toastCtrl.create({
		  	message: msg,
		  	duration: 2000,
		  	position: "top"
	  	}).present();
  	}

  	saveChanges() {
	  	this.is_submit = true;
        console.log('Profile Form Value',this.profileForm.value);
        console.log('VALDATION IS ', this.profileForm.valid);
	 	if(!this.profileForm.valid || !this.city || !this.region || !this.zip) {
			this.presentToast("Please fill-up all necessary fields");
	 	} else {
			let loader = this.loadingCtrl.create();
			loader.present();

		  	let formData = this.profileForm.value;
		  	formData['date_of_birth'] = this.profileForm.value['date_of_birth'];
		  	formData['city'] = this.city;
		  	formData['region'] = this.region;
		  	formData['zip'] = this.zip;
		  	formData['civil_status'] = this.civil_status;
		  	formData['hmo_account_address_id'] = this.hmo_account['address_id'];
            formData['provider_id'] = this.hmo_account['provider_id'];

			this.userProvider.editProfile(formData).then(res => {
				if(res['error'] === 0) {

				let new_sess_hmo_account = this.hmo_account;
				new_sess_hmo_account['civil_status'] = this.civil_status;
				new_sess_hmo_account['place_of_birth'] = this.profileForm.value['place_of_birth'];
				new_sess_hmo_account['date_of_birth'] = this.profileForm.value['date_of_birth'];
				new_sess_hmo_account['height'] = this.profileForm.value['height'];
				new_sess_hmo_account['weight'] = this.profileForm.value['weight'];
				new_sess_hmo_account['company_name'] = this.profileForm.value['company_name'];
				new_sess_hmo_account['company_job_title'] = this.profileForm.value['designation'];
				new_sess_hmo_account['tin_no'] = this.profileForm.value['tin_no'];
				new_sess_hmo_account['zip'] = this.profileForm.value['zip'];
				new_sess_hmo_account['city'] = this.city;
				new_sess_hmo_account['region'] = this.region;
				new_sess_hmo_account['street_address'] = this.profileForm.value['address'];

				let new_sess_user_login = this.user_profile;
				new_sess_user_login['hmo'] = this.hmo_account;
				new_sess_user_login['prefix'] = this.profileForm.value['prefix'];
				new_sess_user_login['firstname'] = this.profileForm.value['firstname'];
				new_sess_user_login['midlename'] = this.profileForm.value['midlename'];
				new_sess_user_login['mothers_maiden_name'] = this.profileForm.value['mothers_maiden_name'];
				new_sess_user_login['lastname'] = this.profileForm.value['lastname'];
				new_sess_user_login['suffix'] = this.profileForm.value['suffix'];
				new_sess_user_login['gender'] = this.profileForm.value['gender'];
                new_sess_user_login['height'] = this.profileForm.value['height'];
                new_sess_user_login['weight'] = this.profileForm.value['weight'];
				new_sess_user_login['date_of_birth'] = this.profileForm.value['date_of_birth'];
                new_sess_user_login['place_of_birth'] = this.profileForm.value['place_of_birth'];
				new_sess_user_login['address_city'] = this.city;
				new_sess_user_login['address_region'] = this.region;
				new_sess_user_login['address_address'] = this.profileForm.value['address'];
				new_sess_user_login['address_zip'] = this.profileForm.value['zip'];

				loader.dismiss();
				this.storage.set("sess_user_hmo_account", new_sess_hmo_account);
					this.storage.set("sess_user_login", new_sess_user_login).then(sess_user_login => {
						//loader.dismiss();
						let alert = this.alertCtrl.create({
							title: 'Edit Profile',
							message: 'Your profile has been updated successfully!',
							cssClass: 'AcceptBookingAlert',
							buttons: [{
							  	text: 'Back to Profile',
							  	role: 'cancel',
							  	handler: () => {
								  	this.navCtrl.pop();
								  	console.log('Cancel clicked');
							  	}
							}]
						});
						alert.present();
					});
				} else {
					loader.dismiss();
					this.presentToast("Something went wrong. Please try again.");
				}
			});
		}
  	}

  	getCity(thisRegion) {
	  	this.is_change_region = true;
        console.log('getCity Function!');
        console.log('getREGIONS:',thisRegion);
	  	this.regions.forEach((cat, id) => {
		  	if(cat.provDesc == thisRegion){
                console.log(thisRegion);
				this.siteProvider.getCitiesByRegions(cat.provCode).then(city => {
					this.cities = city['city'];
					this.is_change_region = false;
					console.log(this.cities);
					this.total_zip_code = 0;
					this.zipcode = null;
				});
		  	}
	  	});
  	}

  	getZipcode() {
	  	console.log('getZipCode',this.city);
	  	this.is_change_city = true;
        if(this.city['citymunDesc']){
            this.city = this.city['citymunDesc']
        }
	  	this.siteProvider.getZipcodeByCity(this.city).then(data => {
		  	if(data['error'] == 0){
				this.total_zip_code = data['total_count'];
				this.zipcode = data['zipcode'];
				this.is_change_city = false;
		  	}
	  	});
  	}

  	addContact(contact=null, contact_idx=null){
		console.log(contact);
		console.log(contact_idx);
		let contactInfoModal = this.modalCtrl.create(ModalInfoPage,{
			modal_type : 'contact_info',
			modal_contact : contact,
			contact_idx: contact_idx
		});

		contactInfoModal.onDidDismiss(data => {
	  		if(data){
				this.user_profile = data;
	  		}
		});
		contactInfoModal.present();
  	}
}
