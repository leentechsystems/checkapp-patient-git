import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BrowseDoctorsPage } from '../../pages/browse-doctors/browse-doctors';
import { DoctorProvider } from '../../providers/doctor/doctor';

/**
 * Generated class for the BrowseSpecialtiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse-specialties',
  templateUrl: 'browse-specialties.html',
})
export class BrowseSpecialtiesPage {

  /* Declare a variable where you'll be storing the data on this page */
  all_results: any;
  current_page = 1; /* Step 1: Declare default page */
  total_count = 0;  /* Total count from the server */
  searched_keyword = "";
  categoriesChks:boolean[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public doctorProvider: DoctorProvider) {
       /* Step 2: Call the function to load the data. First parameter is a refresher (boolean) this is the pull-to-refresh event,
      Second is isLoadNew (boolean) an indicator if we need to get new data from database */
      this.loadAllData(false, true, false);
  }

  loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
      /* getting of data from the provider, we'll passing set of data the function is requiring */
      let thisData = {
        "current_page": this.current_page, /* separate each parameters bty comma*/
        "searched_keyword": this.searched_keyword
      };

      this.doctorProvider.getSpecializations(isLoadNew, thisData).then(data => {
          if(data['error'] == 0) {
              this.total_count = data['result']['total_count'];

              if(!infiniteScroll && infiniteScroll === false) {
                   /* converting into array if variable is empty */
                  /* emptying the variable when refreshed */
                  if(!this.all_results || isLoadNew === true) {
                     this.all_results = [];
                  }
              }

              let tempSelectedChecks = [];
              data['result']['items'].forEach((cat, idx) => {
                    if(cat.id !== undefined) {
                        this.all_results.push(cat);
                    }
                    tempSelectedChecks.push(false);
              });

              this.categoriesChks = tempSelectedChecks;

              if(infiniteScroll) {
                  infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
              }
          }

          if(refresher) {
            refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
          }
      });
  }

   doInfinite(infiniteScroll:any) {
         this.current_page += 1;
         if(this.all_results.length < this.total_count) {
            this.loadAllData(false, false, infiniteScroll);
         } else {
            infiniteScroll.enable(false);
         }
  }

  searchKeyword(onCancel:any) {
      if(onCancel) {
          this.searched_keyword = "";
      }
      
      this.current_page = 1;
      this.all_results = null;
      this.loadAllData(false, true, false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BrowseSpecialtiesPage');
  }

  goToDoctorsList(){
      let selected_specialties = [];

      if(this.all_results) {
          this.all_results.forEach((cat, idx) => {
              cat.checked = this.categoriesChks[idx];
              if(this.categoriesChks[idx] === true) {
                  selected_specialties.push(cat);
              }
          });
      }

      this.navCtrl.push(BrowseDoctorsPage, {
          'all_specialties': this.all_results,
          'selected_specialties': selected_specialties,
      });
  }
}
