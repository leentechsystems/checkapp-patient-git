import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseSpecialtiesPage } from './browse-specialties';

@NgModule({
  declarations: [
    BrowseSpecialtiesPage,
  ],
  imports: [
    IonicPageModule.forChild(BrowseSpecialtiesPage),
  ],
})
export class BrowseSpecialtiesPageModule {}
