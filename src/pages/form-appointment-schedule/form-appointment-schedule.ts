import { Component, ViewChild } from '@angular/core';
import { IonicPage, Tabs, NavController, ModalController, ToastController, LoadingController, NavParams } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { ModalConfirmationAppointmentPage} from '../../pages/modal-confirmation-appointment/modal-confirmation-appointment';
import { TabsPage} from '../../pages/tabs/tabs';
import { AppointmentsCalendarPage} from '../../pages/appointments-calendar/appointments-calendar';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the FormAppointmentSchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-form-appointment-schedule',
	templateUrl: 'form-appointment-schedule.html',
})
export class FormAppointmentSchedulePage {
	@ViewChild('myTabs') tabRef: Tabs;

	date: any;
	daysInThisMonth: any;
	daysInLastMonth: any;
	daysInNextMonth: any;
	monthNames: string[];
	currentMonthIdx: any;
	currentMonth: any;
	currentYear: any;
	currentDate: any;
	currentDay: any;
	todayMonthIdx: any;
	todayMonth: any;
	todayYear: any;
	todayDate: any;
	todayDay: any;
	today: any;
	selected_day: any;
	selected_month: any;
	selected_year: any;
	selected_time: any;
	selected_time_unit: any;
	selected_military_time: any;
	selected_day_of_the_week: any;
	schedules: any;
	available_timeslots: any;
	available_timeslots_loader: boolean = false;
	is_submit: boolean = false;
	dayNames = [];
	dayshortNames = [];
	photos = (this.navParams.get('photos')) ? this.navParams.get('photos') : null;

	consultationForm = this.navParams.get("consultationForm");
	doctor_profile = this.navParams.get("doctorProfile");
	
	constructor(public navCtrl: NavController, public navParams: NavParams, private calendar: Calendar,
				public modalCtrl: ModalController, public doctorProvider: DoctorProvider, public toastCtrl: ToastController,
				public appointmentProvider: AppointmentProvider, public siteProvider: SiteProvider, public loadingCtrl: LoadingController) {

		this.dayNames = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		this.loadAllDoctorSchedPerHospital(true);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad FormAppointmentSchedulePage');
	}

	ionViewWillEnter() {
		this.date = new Date();
		this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		this.dayshortNames = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
		this.getDaysOfMonth();

		this.today = new Date();
		this.todayMonthIdx = this.today.getMonth();
		this.todayMonth = this.monthNames[this.todayMonthIdx];
		this.todayYear = this.today.getFullYear();
		this.todayDate = this.today.getDate();

		/*if no selected date yet, set it today*/
		this.selected_day = this.todayDate;
		this.selected_month = this.todayMonthIdx;
		this.selected_year = this.todayYear;
	}

	getDaysOfMonth() {
		this.daysInThisMonth = new Array();
		this.daysInLastMonth = new Array();
		this.daysInNextMonth = new Array();
		this.currentMonthIdx = this.date.getMonth();
		this.currentMonth = this.monthNames[this.date.getMonth()];
		this.currentYear = this.date.getFullYear();

		var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
		var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
		var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
		var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
		var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;

		if(this.date.getMonth() === new Date().getMonth()) {
			this.currentDate = new Date().getDate();
		} else {
			this.currentDate = 999;
		}
			
		for(var i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
			this.daysInLastMonth.push(i);
		}

		for (var j = 0; j < thisNumOfDays; j++) {
			let day_of_the_week = new Date(this.date.getFullYear(), this.date.getMonth(), j+1).getDay() + 1;
			this.daysInThisMonth.push({
				"day": j+1,
				"day_of_the_week": day_of_the_week
			});
		}

		for (var k = 0; k < (6-lastDayThisMonth); k++) {
			this.daysInNextMonth.push(k+1);
		}

		if(totalDays<36) {
			for(var l = (7-lastDayThisMonth); l < ((7-lastDayThisMonth)+7); l++) {
				this.daysInNextMonth.push(l);
			}
		}
	}

	goToLastMonth() {
		this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
		this.getDaysOfMonth();
	}

	goToNextMonth() {
		this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
		this.getDaysOfMonth();
	}

	selectDate(day, dayOfTheWeek) {
		this.selected_day = day;
		this.selected_month = this.currentMonthIdx;
		this.selected_year = this.currentYear;
		this.selected_day_of_the_week = dayOfTheWeek;

		this.selected_time = null; 
		this.available_timeslots = null; /* reset it */
		this.available_timeslots_loader = true;
		this.getScheduleByDate();
	}

	selectTime(time, time_unit, military_time) {
		console.log('fdsfadfasf');
		this.selected_time = time;
		this.selected_military_time = military_time;
		this.selected_time_unit = time_unit;
	}

	loadAllDoctorSchedPerHospital(isLoadNew) {
		let thisData = {
			"doc_id": this.consultationForm.doctor_id,
			"hospital_id": this.consultationForm.hospital_doctors_hospital_id,
			"selected_day": "",
		};

		this.doctorProvider.getSchedules(isLoadNew, thisData).then((data) => {
			this.schedules = data['timeslot'];
			this.dayNames.forEach((day, idx)=> {
				if(data['timeslot'][day] === null || data['timeslot'][day] === undefined) {
					this.schedules[day] = null;
				} else {
					this.schedules[day] = data['timeslot'][day];
				}
			});
		});
	}

	getScheduleByDate() {
		let convert_to_dayname = this.dayNames[this.selected_day_of_the_week - 1];
		this.available_timeslots = this.schedules[convert_to_dayname];
		this.available_timeslots_loader = false;
		console.log('available_timeslots',this.available_timeslots);
	}

	sendAppointment() {
		this.is_submit = true;
		if(!this.selected_day || !this.selected_year || !this.selected_month || !this.selected_time) {
			this.presentToast("Please set a date and time");
					
		} else {
			let loader = this.loadingCtrl.create();
			loader.present();
						 
			//let split_selected_time = this.selected_time.split(":");
			//let new_selected_time = "";
			let new_selected_day = "";
			let new_selected_month = "";
			let new_selected_year = "";

			/* Convertion to Military time */
			/*if(this.selected_time_unit == "PM") {
				if(parseInt(split_selected_time[0]) < 12) {
					let convert_selected_time = 12 + parseInt(split_selected_time[0]);

					if(convert_selected_time < 10) { 
						new_selected_time = "0"+ convert_selected_time.toString() + ":00:00";
					} else {
						new_selected_time = convert_selected_time.toString() + ":00:00";
					}
				} else {
					new_selected_time = split_selected_time[0] + ":00:00";
				}
			} else {
				if(split_selected_time[0] < 10) {
					new_selected_time += "0"; 
				} else if(split_selected_time[0] === 12) {
					new_selected_time += "00"; 
				} 
				new_selected_time += split_selected_time[0] + ":00:00";
			}*/

			/* Format Month and Day */
			if(this.selected_day < 10) {
				new_selected_day = "0" + this.selected_day;
			} else {
				new_selected_day = this.selected_day;
			}

			if((this.selected_month + 1) < 10) {
				new_selected_month = "0" + (parseInt(this.selected_month) + 1);
			} else {
				new_selected_month = ""+(parseInt(this.selected_month) + 1);
			}

			/* Make a Timestamp, put date and time together */
			let appointment_date = this.selected_year +"-"+ new_selected_month +"-" + new_selected_day +" "+ this.selected_military_time;
			let appointmentForm = this.consultationForm;
			appointmentForm['appointment_date'] = appointment_date;
			console.log(appointmentForm);

			this.appointmentProvider.setAppointment(appointmentForm).then((result) => {
				/* If there are symptoms photos uploaded */
				if(this.photos) {
					this.siteProvider.uploadPhoto(this.photos).then(res => {
						if(res['error'] === 0) {
							let attachments = [];
							res['items'].forEach((items, iidx) => {
								attachments.push({"filename": items.files[0].filename, "filepath": items.files[0].filepath})
							});

							let attachmentsData = {
								"attachment_type": "appointment",
								"item_id": result['result']['appointment_id'],
								"attachment": attachments
							};

							this.siteProvider.saveAttachment(attachmentsData).then(saveAttachmentResult => {
								if(saveAttachmentResult['error'] === 0) {
									console.log("saveAttachmentResult", saveAttachmentResult);
								} else {
									this.presentToast("Something went wrong uploading files please try again.");
								}

								/* Finish this process */
								loader.dismiss();
								this.goToConfirmationModal();
							});
															
							/* If moving of files fails*/     
						} else {
							this.presentToast("Something went wrong uploading files please try again.");
							/* Finish this process */
							loader.dismiss();
							this.goToConfirmationModal();
						}
					});
					
					loader.dismiss();
					this.goToConfirmationModal();

				/* If no symptom photos were uploaded */
				} else {
					/* Finish this process */
					loader.dismiss();
					this.goToConfirmationModal();
				}

			},err => {
				console.log(err);
				this.presentToast("Something went wrong. Please try again later.");
			});
		}
	}

	presentToast(msg) {
		this.toastCtrl.create({
			message: msg,
			duration: 2000,
			position: "top"
		}).present();
	}

	goToConfirmationModal() {
		let modal = this.modalCtrl.create(ModalConfirmationAppointmentPage, {
			"consultationForm": this.consultationForm,
			"doctorProfile": this.doctor_profile,
			"selected_day": this.selected_day,
			"selected_month": this.selected_month,
			"selected_year": this.selected_year,
			"selected_time": this.selected_time,
			"selected_day_of_the_week": this.selected_day_of_the_week,
		});
		modal.onDidDismiss(data => { 
          	//this.navCtrl.setRoot(AppointmentsCalendarPage);
			this.navCtrl.popToRoot();
  			this.navCtrl.parent.select(2);
		});
		modal.present();
	}
}