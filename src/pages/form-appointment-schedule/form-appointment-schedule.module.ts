import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormAppointmentSchedulePage } from './form-appointment-schedule';

@NgModule({
  declarations: [
    FormAppointmentSchedulePage,
  ],
  imports: [
    IonicPageModule.forChild(FormAppointmentSchedulePage),
  ],
})
export class FormAppointmentSchedulePageModule {}
