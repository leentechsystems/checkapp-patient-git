import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HmoAccountDetailsPage } from './hmo-account-details';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    HmoAccountDetailsPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(HmoAccountDetailsPage),
    IonicImageLoader
  ],
})
export class HmoAccountDetailsPageModule {}
