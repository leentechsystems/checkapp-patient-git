import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, LoadingController, NavParams, ToastController, ViewController, Events } from 'ionic-angular';
import { HmoAccountBillDetailsPage } from '../../pages/hmo-account-bill-details/hmo-account-bill-details';
import { FormHmoApplicationPersonalInfoPage} from '../../pages/form-hmo-application-personal-info/form-hmo-application-personal-info';
import { FormHmoPaymentPage} from '../../pages/form-hmo-payment/form-hmo-payment';
import { Storage } from '@ionic/storage';
import { HmoProvider } from '../../providers/hmo/hmo';
import { BrowseHmoTransactionsPage } from '../../pages/browse-hmo-transactions/browse-hmo-transactions';
import { QrCodePage} from '../../pages/qr-code/qr-code';
import { PaymentHistoryModalPage } from '../../pages/payment-history-modal/payment-history-modal';
import { ModalSelectHmoAccountPage } from '../../pages/modal-select-hmo-account/modal-select-hmo-account';


/**
 * Generated class for the HmoAccountDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-hmo-account-details',
	templateUrl: 'hmo-account-details.html',
})
export class HmoAccountDetailsPage {

	hmo_account: any; 
	hmo_total_contract_price: any;
	hmo_total_market_price: any;
	hmo_total_transactions: any;
	current_balance: any;
	today: any;
	today_month: any;
	next_month: any;
	monthNames = [];
	hmo_payment_history: any;

  	payment_history: any;
  	total_payment_history = 0;
  	current_page = 1;
  	paymentMethod: any;

  	hmo_provider: any;
  	hmo_membership_plan: any;

  	from_notif = (this.navParams.get('from_notif')) ? this.navParams.get('from_notif') : null;

	constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
				private storage: Storage, public hmoProvider: HmoProvider, public loadingCtrl: LoadingController,
				public toastCtrl: ToastController, public viewCtrl: ViewController, public events: Events) {
			
		this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		let date = new Date();
		let hours = date.getHours();
		let time_unit = "AM";

		this.today_month = date.getMonth() + 1;
		this.next_month = date.getMonth() + 2;
		if(date.getHours() > 12) {
			hours = hours - 13;
			time_unit = "PM";
		} else {
			time_unit = "AM";
		}

	    if(this.today_month < 10){
	        this.today_month = '0'+this.today_month;
	    }

	    if(this.next_month < 10){
	        this.next_month = '0'+this.next_month;
	    }

		this.today = this.monthNames[date.getMonth()] +" "+ date.getDate() +", "+ date.getFullYear() +", "+ hours +":"+ date.getMinutes() +" "+ time_unit; 
		this.loadNewHmoAccount(false);
		this.loadPaymentTransaction(true);
		this.getPaymentOption();
		this.getHmoProvider();

        events.subscribe('hmo_payment:created', (action, time) => {
			this.reloadData(false);
		});
	}


	ionViewDidEnter() {
      	this.payment_history = null;
		this.loadPaymentTransaction(true);
	}

    reloadData(refresher?){
    	this.hmo_account = null;
      	this.payment_history = null;
		this.loadNewHmoAccount(refresher);
		this.loadPaymentTransaction(true);
		this.getPaymentOption();
		this.getHmoProvider();
    }

	ionViewDidLoad() {
		console.log('ionViewDidLoad HmoAccountDetailsPage');
	}

	loadNewHmoAccount(refresher?) {
		//let loader = this.loadingCtrl.create();
		//loader.present();

		this.storage.get('sess_user_login').then((user) => {
			this.hmoProvider.getUserHmoAccount(true, user).then((hmo) => {
				if(hmo['error'] === 0) {
					this.storage.set("sess_user_hmo_account", hmo['hmo'][0]).then(() => {
						//loader.dismiss();
						this.hmo_account = hmo['hmo'][0];
						this.hmo_account['payment'] = this.payment_history;
											
						let thisData = {
							"billingstart": "2018-4-25",
							"billingend": "2018-5-24",
						};

						console.log(this.hmo_account);

						this.hmoProvider.getUserHmoTransactionsDeductions(thisData).then(res => {
							if(res['error'] === 0) {
								this.hmo_total_contract_price = parseFloat(res['sums']['total_contract_price']);
								this.hmo_total_market_price = parseFloat(res['sums']['total_market_price']);
								this.hmo_total_transactions = res['sums']['items'];
								this.current_balance = this.hmo_total_market_price;
							}
						});
					});

				} else {
					
					this.storage.get("sess_user_hmo_account").then((hmo) => {
						if(hmo){
							this.hmo_account = hmo;
							console.log(this.hmo_account);
						}
					});
				}

	            if(refresher) {
	              refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
	            }

			}, err => {
				console.log("No internet connection");
				//loader.dismiss();
			});
		});
	}

	loadPaymentTransaction(isLoadNew?){

		this.storage.get("sess_user_hmo_account").then((hmo) => {
			if(hmo){
				this.hmo_account = hmo;

				let thisData = {
		        	"hmo_account_id": this.hmo_account.id,
		            "current_page": this.current_page, /* separate each parameters bty comma*/
		        };

		        this.hmoProvider.getPaymentHistoryTransactions(isLoadNew, thisData).then(data => {

		            if(data['error'] === 0) {
		                this.total_payment_history = data['payment_history'] ? data['payment_history']['total_count'] : 0;

						if(!this.payment_history || isLoadNew === true) {
		                    this.payment_history = []; 
		                }

		                if(data['payment_history']){
			              	data['payment_history']['items'].forEach((cat, idx) => {
			                    if(cat.id !== undefined) {
			                    	if(this.payment_history.length < 3){
			                        	this.payment_history.push(cat);
			                    	}
			                    }
			                });
		                }
		            }
				});
			}
		});
	}  

	gotoBillDetails() {
		//this.hmo_account['hmo_provider_attachment'] = this.hmoProvider.base_url+"/assets/static/images/hmo/carewell_logo.jpg";
		this.navCtrl.push(HmoAccountBillDetailsPage, {
			"hmo_account": this.hmo_account,
			"pageType": 'bill_details'
		});
	}

	gotoPaymentDetails(paymentDetails){
		//paymentDetails['hmo_provider_attachment'] = this.hmoProvider.base_url+"/assets/static/images/hmo/carewell_logo.jpg";
		this.navCtrl.push(HmoAccountBillDetailsPage, {
			"hmo_account": paymentDetails,
			"pageType": 'payment_details'
		});
	}

	goToHmoApplicationForm() {
		//this.navCtrl.push(FormHmoApplicationPersonalInfoPage);
		this.modalCtrl.create(ModalSelectHmoAccountPage,{
			'message_type': 'select_hmo_account',
			'hmo_account': this.hmo_account,
		}).present();
	}

	openPaymentOptions() {
		// this.modalCtrl.create(FormHmoPaymentPage).present();
		this.navCtrl.push(FormHmoPaymentPage, {
			"hmo_account": this.hmo_account,
			"payment_method": this.paymentMethod
		});
	}

	goToTransactions() {
		this.navCtrl.push(BrowseHmoTransactionsPage);
	}

	gotoQrCode() {	
		this.navCtrl.push(QrCodePage, {
			"hmo_account": this.hmo_account
		});
	}

	gotoPaymentHistory() {
        this.modalCtrl.create(PaymentHistoryModalPage, {
			"hmo_account": this.hmo_account
        }).present();
  	}

  	getPaymentOption(){
		this.hmoProvider.getPaymentGateway().then((result) => {
			console.log("result", result);
			if(result['error'] == 0){
				this.paymentMethod = result['payment_methods'][0];
			}
		}, err => {
			this.presentToast("Something went wrong. Please try again");
		});
  	}

  	getHmoProvider(){
  		let thisData = {
  			'id' : 1,
  		};
		this.hmoProvider.getHmoProvider(thisData).then((data) => {
			console.log("hmo provider", data);
			if(data['error'] == 0){
				this.hmo_provider = data['hmo'];
				data['hmo_plan'].forEach((cat, idx) => {
					if(cat['is_default'] == 1){
						this.hmo_membership_plan = cat;
					}
				});
			}
		});
  	}

  	presentToast(msg) {
      	this.toastCtrl.create({
         	message: msg,
          	duration: 2000,
          	position: "top"
      	}).present();
  	}

   	gobacktoNotif(){
    	this.viewCtrl.dismiss();
   	}
}
