import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the WalkthroughPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-walkthrough',
  templateUrl: 'walkthrough.html',
})
export class WalkthroughPage {
  @ViewChild(Slides) slides: Slides;
  btn_label = "Next";
  currentIndex = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public siteProvider: SiteProvider, private storage: Storage) {
       // this.siteProvider.getAccessToken().subscribe(data => {
       //      if(data) {
       //          this.storage.set('accessToken', data).then((val) => { });
       //      }
       //  }, err => {
       //  });
       this.siteProvider.getGuestAccessToken();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalkthroughPage');
  }

  slideChanged() {
	    this.currentIndex = this.slides.getActiveIndex();

	   	if(this.currentIndex >= 3) {
	   		  this.btn_label = "Let's get started";
	   	} else {
	   		  this.btn_label = "Next";
	   	}
  }


  nextSlide() {
    if(this.slides.isEnd() && this.currentIndex >= 3) {
        this.navCtrl.push(LoginPage);
  	} else {
        this.slides.slideNext();
    } 
  }

}
