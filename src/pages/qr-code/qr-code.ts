import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QrCodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-qr-code',
  templateUrl: 'qr-code.html',
})
export class QrCodePage {

  	qrData = null;
  	createdCode = null;
  	hmo_account = this.navParams.get("hmo_account");

  	constructor(public navCtrl: NavController, public navParams: NavParams) {
		  console.log(this.hmo_account);
      if(this.hmo_account){
        this.createCode();
      }
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad QrCodePage');
  	}

  	createCode() {
      let thisData = {
        'project': "checkapp",
        'user_id': this.hmo_account.user_id,
        'user_name': this.hmo_account.firstname,
        'account_no': this.hmo_account.account_number,
      };
      let thisdata = JSON.stringify(thisData);
      console.log(thisData);
    	this.createdCode = thisdata;
  	}
}
