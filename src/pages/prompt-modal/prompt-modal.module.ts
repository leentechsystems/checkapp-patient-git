import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromptModalPage } from './prompt-modal';

@NgModule({
  declarations: [
    PromptModalPage,
  ],
  imports: [
    IonicPageModule.forChild(PromptModalPage),
  ],
})
export class PromptModalPageModule {}
