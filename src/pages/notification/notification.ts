import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController, ModalController, Content} from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';
import { SiteProvider } from '../../providers/site/site';
import { AppointmentsCalendarPage} from '../../pages/appointments-calendar/appointments-calendar';
import { BrowseMedicalRecordsPage } from '../../pages/browse-medical-records/browse-medical-records';
import { HmoAccountDetailsPage } from '../../pages/hmo-account-details/hmo-account-details';
import { HmoAccountBillDetailsPage } from '../../pages/hmo-account-bill-details/hmo-account-bill-details';
import { FCM } from '@ionic-native/fcm';
import { Storage } from '@ionic/storage';
import { HmoProvider } from '../../providers/hmo/hmo';
/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-notification',
	templateUrl: 'notification.html',
})
export class NotificationPage {
	@ViewChild(Content) content: Content;

	notifications: any;
	current_page = 1;
	total_notifications = 0;
	hmo_account : any;

	constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider,
				public events: Events, public fcm: FCM, public toastCtrl: ToastController, public modalCtrl: ModalController,
				public storage: Storage, public hmoProvider: HmoProvider, public siteProvider: SiteProvider) {

		// events.subscribe('total_unread_notification:created', (action, time) => {
		// 	console.log(action);
		// 	this.ionViewDidEnter();
		// });
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad NotificationPage');

		this.storage.get("sess_user_hmo_account").then((hmo) => {
			if(hmo){
				this.hmo_account = hmo;
				console.log(this.hmo_account);
			}else{
				this.storage.get('sess_user_login').then((user) => {
					this.hmoProvider.getUserHmoAccount(true, user).then((hmo) => {
						if(hmo['error'] === 0) {
							this.storage.set("sess_user_hmo_account", hmo['hmo'][0]).then(() => {
								this.hmo_account = hmo['hmo'][0];
							});
						}
					});
				});
			}
		});
	}

	ionViewDidEnter() {
		this.notifications = null;
		this.current_page = 1;
		this.getNotification(false, true, false);
	}

	reloadData(refresher?){
		this.notifications = null;
		this.current_page = 1;
		this.getNotification(refresher, true, false);
	}

	/* from rhan */
	getNotification(refresher?, isLoadNew?, infiniteScroll?){

		let thisData = {
			"current_page": this.current_page
		};

		this.userProvider.getNotification(isLoadNew, thisData).then(data => {
			if(data['error'] === 0) {
				this.total_notifications = data['result']['total_count'];

				if(!infiniteScroll || infiniteScroll === false) {
					if(!this.notifications || isLoadNew === true) {
						this.notifications = [];
					}
				}

				data['result']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						this.notifications.push(cat);
					}
				});
				if(infiniteScroll) {
					infiniteScroll.complete();
				}
			}

			if(refresher) {
				refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
			}
		});
	}

	doInfinite(infiniteScroll:any) {
		this.current_page += 1;
		console.log(this.notifications);
		if(this.notifications.length < this.total_notifications) {
			this.getNotification(false, false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
	}

	gotoAppointmentCalendar(this_index, notification_id, this_status, temp_notification) {
		console.log('sdada');
		console.log('temp_notification',temp_notification);
		if(this_status == '2'){
			let thisData = {
				"id": notification_id,
				"status": '1'
			};

			this.userProvider.changeNotificationStatus(thisData).then(data => {
				if(data['error'] == 0){
					if(data['message'] == 'Success.'){
						this.events.publish('total_unread_notification:created', 'summation', Date.now());

						if(this.notifications){
							this.notifications.forEach((pGig, idx) => {
								if(idx == this_index){
									pGig['status'] = '1';
								}
							});
						}
					}
				}
			});
		}
		if(temp_notification['type'] == 'hmo_transactions'){
			if(temp_notification['action'] == 'complete'){
				let doneModal = this.modalCtrl.create(BrowseMedicalRecordsPage,{
					from_notif : 'true'
				});

				doneModal.onDidDismiss(data => {
					if(data){

					}
				});

				doneModal.present();
			}else{
				let doneModal = this.modalCtrl.create(AppointmentsCalendarPage,{
					from_notif : 'true'
				});

				doneModal.onDidDismiss(data => {
					if(data){

					}
				});
				doneModal.present();
			}
		}else if(temp_notification['type'] == 'hmo_account'){
			if(temp_notification['action'] == 'hmo_account_activated' || temp_notification['action'] == 'reject_payment'){
				let doneModal = this.modalCtrl.create(HmoAccountDetailsPage,{
					from_notif : 'true'
				});

				doneModal.onDidDismiss(data => {
					if(data){

					}
				});

				doneModal.present();

			}
		}else if(temp_notification['type'] == 'payment_history'){
			if(temp_notification['action'] == 'approve_payment' || temp_notification['action'] == 'reject_payment'){

				let thisData = {
		        	"payment_history_id": temp_notification['item_id']
		        };

		        this.hmoProvider.getPaymentHistoryById(thisData).then(data => {
		            if(data['error'] === 0) {
		                if(data['payment_history']){
			              	if(data['payment_history']['total_count']){
			              		let doneModal = this.modalCtrl.create(HmoAccountBillDetailsPage,{
									"hmo_account": data['payment_history']['items'][0],
									"pageType": 'payment_details',
									"from_notif" : 'true'
								});

								doneModal.onDidDismiss(data => {
									if(data){

									}
								});
								doneModal.present();
			              	}
		                }
		            }
				});
			}
		}
	}
	/* end */
}
