import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationPage } from './notification';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    NotificationPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(NotificationPage),
    IonicImageLoader,
  ],
})
export class NotificationPageModule {}
