import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookmarkPage } from './bookmark';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    BookmarkPage,
  ],
  imports: [
  	PipesModule,
    IonicPageModule.forChild(BookmarkPage),
  ],
})
export class BookmarkPageModule {}
