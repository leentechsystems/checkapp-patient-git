import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormFilterHospitalsPage } from './form-filter-hospitals';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  declarations: [
    FormFilterHospitalsPage,
  ],
  imports: [
    IonicSelectableModule,
    IonicPageModule.forChild(FormFilterHospitalsPage),
  ],
})
export class FormFilterHospitalsPageModule {}
