import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalSelectHmoAccountPage } from './modal-select-hmo-account';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    ModalSelectHmoAccountPage,
  ],
  imports: [
  	PipesModule,
    IonicPageModule.forChild(ModalSelectHmoAccountPage),
  ],
})
export class ModalSelectHmoAccountPageModule {}
