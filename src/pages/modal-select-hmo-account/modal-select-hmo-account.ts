import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { HmoAccountDetailsPage } from '../../pages/hmo-account-details/hmo-account-details';
import { FormHmoApplicationPersonalInfoPage} from '../../pages/form-hmo-application-personal-info/form-hmo-application-personal-info';

import { HmoProvider } from '../../providers/hmo/hmo';
/**
 * Generated class for the ModalSelectHmoAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-modal-select-hmo-account',
	templateUrl: 'modal-select-hmo-account.html',
})
export class ModalSelectHmoAccountPage {

	activeButton = "add";
	photos = this.navParams.get('photos') ? this.navParams.get('photos') : null;
	paymentForm = this.navParams.get("paymentForm");
	paymentMethod = this.navParams.get("paymentMethod");
	hmo_account = this.navParams.get("hmo_account");
	message_type: any;
	hmo_provider: any;
	hmo_membership_plan: any;

	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
		public hmoProvider: HmoProvider) {
		this.message_type = this.navParams.get('message_type');
  		console.log(this.photos);
  		console.log(this.paymentForm);
  		console.log(this.hmo_account);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ModalSelectHmoAccountPage');
		this.getHmoProvider();
	}

	makeAnAction(thisAction) {
		if(thisAction === "cancel") {
			this.viewCtrl.dismiss();
		} else {
			if(this.hmo_provider && this.hmo_membership_plan){
				if(this.message_type == 'select_hmo_account'){
					this.navCtrl.push(FormHmoApplicationPersonalInfoPage, {
						"action": this.activeButton,
						"hmo_provider": this.hmo_provider,
						"hmo_membership_plan": this.hmo_membership_plan,
						"hmo_account": this.hmo_account,
					});
				}else{
					this.viewCtrl.dismiss(thisAction);
				}
			}
		}
	}

  	getHmoProvider(){
  		let thisData = {
  			'id' : 1,
  		};
		this.hmoProvider.getHmoProvider(thisData).then((data) => {
			console.log("hmo provider", data);
			if(data['error'] == 0){
				this.hmo_provider = data['hmo'];
				data['hmo_plan'].forEach((cat, idx) => {
					if(cat['is_default'] == 1){
						this.hmo_membership_plan = cat;
					}
				});
			}
		});
  	}

	escapeModal() {
		this.viewCtrl.dismiss();
	}

	stopPropagation(e) {
	  e.stopPropagation();
	}

	changeActive(thisActive) {
		this.activeButton = thisActive;
	}
}
