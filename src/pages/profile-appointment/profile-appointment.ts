import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ProfileDoctorPage } from '../../pages/profile-doctor/profile-doctor';
import { AppointmentProvider } from '../../providers/appointment/appointment';

/**
 * Generated class for the ProfileAppointmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-appointment',
  templateUrl: 'profile-appointment.html',
})
export class ProfileAppointmentPage {

  details = this.navParams.get('appointment_details');
  qrData = null;
  createdCode = null;
  from_trans = (this.navParams.get('from_trans')) ? this.navParams.get('from_trans') : null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
              public appointmentProvider: AppointmentProvider) {
    this.createCode();
    console.log(this.details);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileAppointmentPage');
  }

  escapeModal() {
      this.viewCtrl.dismiss();
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  createCode() {
    this.createdCode = this.details.id;
  }

  viewProfile() {
  	//this.viewCtrl.dismiss();
  	this.details['hospital_address'] = this.details['hospital_address_address'];
  	this.details['hospital_city'] = this.details['hospital_address_city'];
  	this.details['hospital_region'] = this.details['hospital_address_region'];
  	this.details['hospital_zip'] = this.details['hospital_address_zip'];
  	this.details['hospital_country'] = this.details['hospital_address_country'];

  	this.details['prefix'] = this.details['doctor_prefix'];
  	this.details['suffix'] = this.details['doctor_suffix'];
  	this.details['firstname'] = this.details['doctor_firtname'];
  	this.details['lastname'] = this.details['doctor_lastname'];
  	this.details['medical_degree'] = this.details['doctor_medical_degree'];
  	this.details['id'] = parseInt(this.details['doctor_id']);
  	this.details['hospital_latitude'] = this.details['hospital_address_lat'];
  	this.details['hospital_longtitude'] = this.details['hospital_address_lng'];

  	this.navCtrl.push(ProfileDoctorPage, {
  		"hospital_profile": this.details
  	});
  }

}
