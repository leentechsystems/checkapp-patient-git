import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePrescriptionPage } from './profile-prescription';

@NgModule({
  declarations: [
    ProfilePrescriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePrescriptionPage),
  ],
})
export class ProfilePrescriptionPageModule {}
