import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { ProfileHospitalsPage } from './../profile-hospitals/profile-hospitals';
import { FormFilterHospitalsPage } from './../form-filter-hospitals/form-filter-hospitals';

import { Storage } from '@ionic/storage';

import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the BrowseHospitalsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse-hospitals',
  templateUrl: 'browse-hospitals.html',
})
export class BrowseHospitalsPage {

  accessToken:any = "";
  all_hospitals: any;
  current_page = 1;
  total_count = 0;
  searched_keyword = (this.navParams.get('searched_keyword')) ? this.navParams.get('searched_keyword') : "";

  specialties: any;
  selected_specialties: any;
  applied_filters: any;
  current_location: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
              public hospitalsProvider: HospitalsProvider, private storage: Storage,
              public userProvider: UserProvider) {
      
      this.storage.get("sess_current_location").then(sess_current_location => {
          if(sess_current_location) {
              this.current_location = sess_current_location;
          }
          this.loadAllData(false, true, false);
      }); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BrowseHospitalsPage');
  }

  gotoProfileHospital(hospital_profile) {
    this.navCtrl.push(ProfileHospitalsPage, {
      "hospital_profile": hospital_profile
    });
  }

  openFilterForm() {
    let filtermodal = this.modalCtrl.create(FormFilterHospitalsPage, {
        "specialties": this.specialties
    });

    filtermodal.onDidDismiss(data => { 
        if(data) {
            this.applied_filters = data;
            if(this.applied_filters.city) {
              this.all_hospitals = null;
              this.current_page = 1;
            }
            // this.searched_keyword = this.applied_filters.city;
            this.loadAllData(false, true, false);
        }
    });

    filtermodal.present();
    
  }

  loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
      
      console.log(this.current_page);

      let origin_coordinates = {};

      if(this.current_location) {
        origin_coordinates = {
          "lat": this.current_location['lat'],
          "lng": this.current_location['lng'],
        };
      } else {
        origin_coordinates = {
          "lat": 0,
          "lng": 0,
        };
      }

      let thisData = {
        "current_page": this.current_page,
        "searched_keyword": this.searched_keyword,
        "applied_filters": this.applied_filters,
        "origin_coordinates": origin_coordinates
      };

      this.hospitalsProvider.getAllHospitals(isLoadNew, thisData).then(data => {
        if(data['error'] == 0){

          this.total_count = data['result']['total_count'];

          if(!infiniteScroll && infiniteScroll === false) {
            if(!this.all_hospitals || isLoadNew === true) {
               this.all_hospitals = []; 
            }
          }

          data['result']['items'].forEach((cat, idx) => {
              if(cat.id !== undefined) {
                  this.all_hospitals.push(cat);
              }
          });
          
          if(infiniteScroll) {
            infiniteScroll.complete();
          }
        }

        if(refresher) {
          refresher.complete();
        }
    });
  }

  doInfinite(infiniteScroll:any) {
         this.current_page += 1;
         if(this.all_hospitals.length < this.total_count) {
            this.loadAllData(false, false, infiniteScroll);
         } else {
            infiniteScroll.enable(false);
         }
  }

  searchNews(onCancel:any) {
    if(onCancel) {
          this.searched_keyword = "";
      }
      this.all_hospitals = null;
      this.current_page = 1; 
      this.loadAllData(false, true, false); 
  }

  onTyping(){
    this.searched_keyword = "";
  }
  
  reloadData(refresher?){
      this.all_hospitals = null;
      this.current_page = 1;
      this.loadAllData(refresher, true, false);
  }

  addtoBookmark(temp_hospital, hospital_index){
    console.log('hospital',temp_hospital);
    console.log('index',hospital_index);

    let thisData = {
      "item_id": temp_hospital.id,
      "type": 'hospital',
    };

    if(temp_hospital.bookmark_id){
      thisData['id'] = temp_hospital.bookmark_id;
    }

    this.all_hospitals.forEach((cat, idx) => {
      if(hospital_index == idx){
        if(temp_hospital.bookmark_id){
          cat.bookmark_id = null;
        }else{
          cat.bookmark_id = 1;
        }
      }
    });

    this.userProvider.saveBookmarks(thisData).then((data) => {

      if(data['error'] === 0){
        this.all_hospitals.forEach((cat, idx) => {
          if(hospital_index == idx){
            if(thisData['id']){
              cat.bookmark_id = null;
            }else{
              cat.bookmark_id = data['result']['id'];
            }
          }
        });
      }

    }, err => {

      console.log(err);

    });
  }
}
