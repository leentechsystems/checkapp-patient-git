import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseHospitalsPage } from './browse-hospitals';

@NgModule({
  declarations: [
    BrowseHospitalsPage,
  ],
  imports: [
    IonicPageModule.forChild(BrowseHospitalsPage),
  ],
})
export class BrowseHospitalsPageModule {}
