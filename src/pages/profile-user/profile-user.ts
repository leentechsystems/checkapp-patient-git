import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ActionSheetController, LoadingController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HmoAccountDetailsPage } from '../../pages/hmo-account-details/hmo-account-details';
import { HmoProvider } from '../../providers/hmo/hmo';
import { SiteProvider } from '../../providers/site/site';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera } from '@ionic-native/camera';
import { ChangePasswordPage } from './../change-password/change-password'; /* 04-20-2018 */
import { EditProfilePage } from './../edit-profile/edit-profile';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the ProfileUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-profile-user',
	templateUrl: 'profile-user.html',
})
export class ProfileUserPage {

	user_profile = this.navParams.get('user_profile');
	userid: any;
	user_name: any;
	hmo_account: any;
	current_location: any; 
	photo: any;
	is_uploading = false;
	contact: any;
	privacy: any;
	no_current_location: any;

	constructor(public navCtrl: NavController, 
		public navParams: NavParams, 
		public storage: Storage,
		public hmoProvider: HmoProvider, 
		public siteProvider: SiteProvider, 
		public toastCtrl: ToastController,
		public camera: Camera, 
		public imagePicker: ImagePicker, 
		public actionSheetCtrl: ActionSheetController,
		public loadCtrl: LoadingController, public userProvider: UserProvider, 
		public events: Events) {

		if(this.user_profile.medical_privacy){
			if(this.user_profile.medical_privacy == '1'){
				this.privacy = true; 
			}
			else{
				this.privacy = false;
			}
		}
		if(this.user_profile.medical_privacy == '1'){
			this.privacy = true; 
		}
		else{
			this.privacy = false;
		}

		events.subscribe('refresh:mylocation', (locationData, time) => {
			this.storage.get("sess_current_location").then(sess_current_location => {
				if(sess_current_location) {
					this.current_location = sess_current_location;
				} else {
					this.siteProvider.detectCurrentLocation();
				}
			});
		});

		events.subscribe('cant_get_location:mylocation', (data, time) => {
			this.no_current_location = true;
		});
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ProfileUserPage');
			
		this.storage.get('sess_user_login').then((user) => {
			this.userid = user.id;
			this.user_name = user.firstname;
			this.contact = this.user_profile['contact'];

			if(user.attachments_filename && user.attachments_filepath) {
				this.photo = this.siteProvider.base_url + user.attachments_filepath +"/"+ user.attachments_filename;
			}

			this.hmoProvider.getUserHmoAccount(true, user).then((hmo) => {
				console.log(hmo);
				if(hmo['error'] == 0){
					 this.storage.set("sess_user_hmo_account", hmo['hmo'][0]);
					 this.hmo_account = hmo['hmo'][0];
					 this.user_profile.hmo = hmo['hmo'];
					 console.log('hmo_account',this.hmo_account);
					 console.log('hmoProvide',this.hmoProvider);
					 console.log('user_profile',this.user_profile);
				}else{
					this.storage.get("sess_user_hmo_account").then(hmo => {
						if(hmo) {
							this.hmo_account = hmo;
						}
					});
				}
			}, err => {
				console.log(err);
			});

			this.storage.get("sess_current_location").then(currentloc => {
				if(currentloc) {
					this.current_location = currentloc;
				}else {
					this.siteProvider.detectCurrentLocation();
				}
			});
		});

		if(!this.current_location && !this. no_current_location){
			console.log('this.events.publish');
	        setTimeout( () => {
	        	this.events.publish('cant_get_location:mylocation');
	        }, 5000);
		}
	}

	goToHmoAccountDetails(thisHMO) {
		this.storage.get("sess_user_hmo_account").then((hmo) => {
			if(hmo) {
				this.navCtrl.push(HmoAccountDetailsPage, {
					"hmo_account": hmo
				});    
			}
		});
	}

	presentActionSheet() {
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Select Image Source',
			buttons: [{
				text: 'Load from Library',
				handler: () => { this.openImagePicker(); }
			},
			{
				text: 'Use Camera',
				handler: () => { this.takePicture(); }
			},
			{
				text: 'Cancel',
				role: 'cancel'
			}]
		});
		actionSheet.present();
	}

	 openImagePicker(){
	 	console.log('here1');

		let options= {
			maximumImagesCount: 1,
			quality: 30,
		}

	 	console.log('here2');
		this.imagePicker.getPictures(options).then((results) => {
	 		console.log('results',results);
			if(results !== 'OK'){
				results.forEach((pp, pidx) => {
					let pp_pieces = pp.split("/");
					let new_pp = {
						"name": pp_pieces[pp_pieces.length - 1],
						"path": pp
					};

					let thisPhoto = [];
					thisPhoto.push(new_pp);
					this.uploadPhoto(thisPhoto);
				});
			}
		}, (err) => { console.log(err) });
	}

	takePicture(){
		let options = {
			quality: 30,
			correctOrientation: true
		};

		this.camera.getPicture(options).then((data) => {
			let pp_pieces = data.split("/");
			let new_pp = {
				"name": pp_pieces[pp_pieces.length - 1],
				"path": data
			};

			let thisPhoto = [];
			thisPhoto.push(new_pp);
			this.uploadPhoto(thisPhoto);

		}, function(error) {
			console.log(error);
		});
	}

	uploadPhoto(thisPhoto) {
		this.is_uploading = true;

		this.siteProvider.uploadPhoto(thisPhoto).then(res => {
			if(res['error'] === 0) {
				let attachments = [];
				res['items'].forEach((items, iidx) => {
					attachments.push({"filename": items.files[0].filename, "filepath": items.files[0].filepath})
				});

				let attachmentsData = {
					"attachment_type": "user",
					"item_id": this.userid,
					"attachment": attachments
				};

				this.siteProvider.saveAttachment(attachmentsData).then(saveAttachmentResult => {
					if(saveAttachmentResult['error'] === 0) {
						let new_user_profile = this.user_profile;
						new_user_profile['attachments_filename'] = saveAttachmentResult['attachments'][0]['filename'];
						new_user_profile['attachments_filepath'] = saveAttachmentResult['attachments'][0]['filepath'];

						this.storage.set("sess_user_login", new_user_profile).then(sess_user_login => {
							this.user_profile = new_user_profile;
							this.photo = this.siteProvider.base_url + saveAttachmentResult['attachments'][0]['filepath'] + "/" + saveAttachmentResult['attachments'][0]['filename'];
						});

					} else {
						this.presentToast("Something went wrong uploading files please try again.");
					}
					/* Finish this process */
					this.is_uploading = false;
				});
				
			/* If moving of files fails*/     
			} else {
				this.presentToast("Something went wrong uploading files please try again.");
				/* Finish this process */
				this.is_uploading = false;
			}
		});
	}  

	editSection(thisSection) {
		this.navCtrl.push(EditProfilePage, {
			"editing_section": thisSection,
			"user_profile": this.user_profile,
			"hmo_account": this.hmo_account,
			"contact": this.user_profile['contact'],
		});
	}

	presentToast(msg) {
		this.toastCtrl.create({
			message: msg,
			duration: 2000,
			position: "top"
		}).present();
	}

	/* 04-20-2018 */
	changePass(){
		this.navCtrl.push(ChangePasswordPage);
	}

	privacyToggle(){
		console.log('privacy',this.privacy);

		let loader = this.loadCtrl.create({
			content: "Saving..."
		});
		loader.present();

		let priv;
		if(this.privacy == true){
			priv = '1';
		}
		else{
			priv = '0';
		}

		let thisData = {
			privacy: priv
		};

		this.userProvider.editMedicalPrivacy(thisData).then(data => {
			console.log(data);
			if(data['error'] === 0){
				loader.dismiss();
				if(priv == '1'){
					this.user_profile['medical_privacy'] = '1';
					let temp_userProfile = this.user_profile;
					this.storage.set("sess_user_login", temp_userProfile);
				}else{
					this.user_profile['medical_privacy'] = '0';
					let temp_userProfile = this.user_profile;
					this.storage.set("sess_user_login", temp_userProfile);
				}
				this.siteProvider.showToast('Medical Privacy Saved.');
			}
			else{
				loader.dismiss();
				this.siteProvider.showToast(data['message']);
			}
		});
	}

}
