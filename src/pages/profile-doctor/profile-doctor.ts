import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, ModalController, AlertController, LoadingController, NavParams, Platform, App, ToastController } from 'ionic-angular';
import { FormAppointmentConsultationDetailsPage } from '../../pages/form-appointment-consultation-details/form-appointment-consultation-details';
import { ModalSelectHmoAccountPage } from '../../pages/modal-select-hmo-account/modal-select-hmo-account';

import { Storage } from '@ionic/storage';

import { DoctorProvider } from '../../providers/doctor/doctor';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { SiteProvider } from '../../providers/site/site';
import { HmoProvider } from '../../providers/hmo/hmo';
import { UserProvider } from '../../providers/user/user';

import { GoogleMap, GoogleMaps } from '@ionic-native/google-maps';
import { FormHmoPaymentPage } from '../../pages/form-hmo-payment/form-hmo-payment';
import { HmoAccountDetailsPage } from '../../pages/hmo-account-details/hmo-account-details';
import { PromptModalPage } from '../../pages/prompt-modal/prompt-modal';

/**
 * Generated class for the ProfileDoctorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 declare var google;

@IonicPage()
@Component({
	selector: 'page-profile-doctor',
	templateUrl: 'profile-doctor.html',
})
export class ProfileDoctorPage {

	hospital_tabs: string = '';
	hospital_id: any;
	doc_id: any;
	all_scheds: any;
	hospital_location: any;
	hospital_services: any;
	total_hospital_services = 0;
	total_consultation_services = 0;
	total_count = 0;
	days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
	total_storeScheds = 0;
	storeScheds: any;
	current_page = 1;
	marker: any;
	contactNumber: any;
	contacts: any;
	selected_service: any;
	map_style = "";

	x = 0;

	doctorProfile = this.navParams.get('doctorProfile');

	@ViewChild('map') mapElement: ElementRef;
	map: GoogleMap;

	main_hospital: any;
	more_hospital = [];
	hospitals: any;

	alert_title: any;
	alert_desc: any;
	btn_txt = 'Okay';
	paymentMethod: any;
	hmo_account: any;

	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				private appCtrl:App,
				public toastCtrl: ToastController,
				public doctorProvider: DoctorProvider,
				private storage: Storage,
				public modalCtrl: ModalController,
				public alertCtrl: AlertController,
				public siteProvider: SiteProvider,
				public loadingCtrl: LoadingController,
				public platform: Platform,
				private googleMaps: GoogleMaps,
				public hospitalsProvider: HospitalsProvider,
				public hmoProvider: HmoProvider,
				public userProvider: UserProvider) {

		if (this.hospital_tabs == ''){
			this.hospital_tabs = 'hospital';
			this.map_style = "display: block;";
		}

		this.doc_id = this.doctorProfile['user_id'];
		this.hospital_id = this.doctorProfile.hospital_doctors_hospital_id;
		this.hospitals = this.doctorProfile['hospital'];

		let no_main = 'no';
		this.hospitals.forEach((hosp, idx) => {
			if(hosp['main'] == '1'){
				no_main = 'yes';
				this.main_hospital = hosp;
				this.hospital_id = hosp['hospital_id'];
				this.more_hospital.push(hosp);
			}else{
				if(hosp['hospital_id'] != '0'){
					this.more_hospital.push(hosp);
				}
			}
		});

		if(no_main == 'no'){
			this.main_hospital = [];
			this.more_hospital = [];
			this.hospitals.forEach((hosp, idx) => {
				if(idx == 0){
					this.main_hospital = hosp;
					if(this.hospitals.length != 1){
						this.more_hospital.push(hosp);
					}
				}else{
					this.more_hospital.push(hosp);
				}
			});
		}

		this.storage.get("sess_user_hmo_account").then((hmo) => {
			console.log('hmo',hmo);
			if(hmo) {
				this.hmo_account = hmo;
			}
		 });

		this.loadAllData(false, true, false);
		this.getHospitalServices();
		this.getPaymentOption();
		console.log('main_hospital',this.main_hospital);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ProfileDoctorPage');
		if(this.main_hospital.hospital_latitude && this.main_hospital.hospital_longtitude) {
		 	this.initializeMap();
			this.getDistanceTime();
		}
	}

	reloadData(){
		this.storeScheds = null;
		this.total_storeScheds = 0;
		this.loadAllData(false, true, false);
		this.getHospitalServices();

		console.log('ionViewDidLoad ProfileDoctorPage');
		if(this.main_hospital.hospital_latitude && this.main_hospital.hospital_longtitude) {
			this.initializeMap();
			this.getDistanceTime();
		}
	}

	getDistanceTime() {
		this.siteProvider.detectCurrentLocation();
		/*this.siteProvider.getLocationInfoFromGoogle(parseFloat(this.main_hospital['hospital_latitude']), parseFloat(this.main_hospital['hospital_longtitude'])).then(location => {
			if(location['status'] === "OK") {
				if(location['rows'][0]['elements'][0]['status'] !== "ZERO_RESULTS") {
					this.hospital_location = location['rows'][0];
					this.hospital_location = this.hospital_location['elements'][0];
				}
			}
		});*/
	}

 	loadAllData(refresher?, isLoadNew?, infiniteScroll?) {

		let thisData = {
			"hospital_id": this.hospital_id,
			"doc_id": this.doc_id,
			"selected_day": ""
		};

		 this.doctorProvider.getSchedules(isLoadNew, thisData).then(data => {
             this.storeScheds = [];
            if(data['error'] === 0) {
				this.total_storeScheds = data['total_count'];
				this.all_scheds = data['timeslot'];
				console.log(this.all_scheds);

				this.days.forEach((days, didx) => {
					if(this.total_storeScheds > 0) {
						if(!this.storeScheds) {
							this.storeScheds = [];
						}
						if(this.all_scheds[days] !== undefined){
							let newSched = {
								"day" : days,
								"timeslots" : this.all_scheds[days]
							};
							this.storeScheds.push(newSched);
						}
						this.x++;
					}
				});

				console.log('total_storeScheds',this.total_storeScheds);
				console.log('storeScheds',this.storeScheds);
                console.log('from API',data['timeslot']);
			}

			if(refresher) {
				refresher.complete();
			}
		});
	}

	openHmoAccount() {
		this.modalCtrl.create(ModalSelectHmoAccountPage,{
			'message_type': 'select_hmo_account',
			'hmo_account': this.hmo_account,
		}).present();
	}

	getPaymentOption(){
		this.hmoProvider.getPaymentGateway().then((result) => {
			console.log("result", result);
			if(result['error'] == 0){
				this.paymentMethod = result['payment_methods'][0];
			}
		}, err => {
			this.presentToast("Something went wrong. Please try again");
		});
	}

	setAppointment(){
		if(this.hmo_account) {
			if(this.hmo_account['status'] === "1" && this.hmo_account['payment_status'] === "1") {
				console.log(this.doctorProfile);
				//if(this.selected_service) {
					this.navCtrl.push(FormAppointmentConsultationDetailsPage, {
						"doctorProfile": this.doctorProfile,
						"consultation_type": this.hospital_services ? this.hospital_services['consultation'] : null
					});
				//}
			} else {
				this.alert_title = this.hmo_account['payment_title'];
				this.alert_desc = this.hmo_account['payment_note'];

				if(this.hmo_account['status'] == '4'){
					if(this.hmo_account['payment_status'] == '0'){
						this.btn_txt = 'Add Again';
					}
				}

				let alert = this.alertCtrl.create({
						title: this.alert_title,
						message: this.alert_desc,
						cssClass: 'AcceptBookingAlert',
						buttons: [{
								text: 'Not now',
								role: 'cancel',
						},{
								text: this.btn_txt,
								handler: () => {
									console.log('Cancel clicked');

									if(this.hmo_account['status'] == "2" || this.hmo_account['status'] == "4"){
										if(this.hmo_account['payment_status'] == '0'){
											this.openHmoAccount();
										}else{
											this.appCtrl.getRootNav().push(HmoAccountDetailsPage);
										}

									}else{
										this.appCtrl.getRootNav().push(FormHmoPaymentPage, {
											"hmo_account": this.hmo_account,
											"payment_method": this.paymentMethod
										});
									}
								}
						}]
				});
				alert.present();
			}

		} else {
			this.openHmoAccount();
		}
	}

	initializeMap() {
		let element = this.mapElement.nativeElement;
		this.map = new google.maps.Map(this.mapElement.nativeElement, {
			zoom: 12,
			center: {lat: parseFloat(this.main_hospital['hospital_latitude']), lng: parseFloat(this.main_hospital['hospital_longtitude'])},
			mapTypeControl: false,
			streetViewControl: false
		});

		this.marker = new google.maps.Marker({
			map: this.map,
			animation: google.maps.Animation.DROP,
			position: {lat: parseFloat(this.main_hospital['hospital_latitude']), lng: parseFloat(this.main_hospital['hospital_longtitude'])},

		});
	}

	getHospitalServices() {
		let thisData = {
			'hospital_id': this.hospital_id,
			'doctor_id': this.doctorProfile['user_id']
		};

		this.doctorProvider.getUserServices(thisData).then(services => {
			if(services['error'] === 0) {
				console.log('services',services);
				this.total_hospital_services = services['services']['service_count'];
				this.total_consultation_services = services['services']['total_count']['consultation'];
				this.hospital_services = services['services']['data'];
				this.total_count = services['services']['total_count'];
			}
		}, err => {
			console.log("getServices", err);
		});
	}

	changeMapVisibility() {
		if(this.hospital_tabs === 'hospital') {
			this.map_style = "display: block;";
		} else {
			this.map_style = "display: none;";
		}
	}

	viewHospital(hospital, thisIndex){
		// let loader = this.loadingCtrl.create();
		// loader.present();

		// this.more_hospital.splice(thisIndex, 1);
		// this.more_hospital.push(this.main_hospital);

		// this.main_hospital = hospital;
		// this.ionViewDidLoad();
		// loader.dismiss();

		console.log(hospital);

		let loading = this.loadingCtrl.create({
			spinner: 'crescent',
			duration: 1000
		});

		this.doctorProfile['hospital_address'] = hospital.hospital_address;
		this.doctorProfile['hospital_address_id'] = hospital.hospital_address_id;
		this.doctorProfile['hospital_attachments_filename'] = hospital.hospital_attachments_filename;
		this.doctorProfile['hospital_attachments_filepath'] = hospital.hospital_attachments_filepath;
		this.doctorProfile['hospital_city'] = hospital.hospital_city;
		this.doctorProfile['hospital_country'] = hospital.hospital_country;
		this.doctorProfile['hospital_description'] = hospital.hospital_description;
		this.doctorProfile['hospital_doctors_hospital_id'] = hospital.hospital_id;
		this.doctorProfile['hospital_doctors_main'] = hospital.main;
		this.doctorProfile['hospital_email'] = hospital.hospital_email;
		this.doctorProfile['hospital_latitude'] = hospital.hospital_latitude;
		this.doctorProfile['hospital_longtitude'] = hospital.hospital_longtitude;
		this.doctorProfile['hospital_name'] = hospital.hospital_name;
		this.doctorProfile['hospital_region'] = hospital.hospital_region;
		this.doctorProfile['hospital_zip'] = hospital.hospital_zip;

		loading.onDidDismiss(() => {
			//this.more_hospital.splice(thisIndex, 1);
			//this.more_hospital.push(this.main_hospital);
			this.hospital_id = hospital.hospital_id;
			this.main_hospital = hospital;
			this.reloadData();
		});

		loading.present();

	}

	presentToast(msg) {
		this.toastCtrl.create({
			message: msg,
			duration: 2000,
			position: "top"
		}).present();
	}

	addtoBookmark(temp_doctor){
		console.log('doctor',temp_doctor);

		let thisData = {
			"item_id": temp_doctor.user_id,
			"type": 'doctor',
		};

		if(temp_doctor.bookmark_id){
			thisData['id'] = temp_doctor.bookmark_id;
		}

		if(temp_doctor.bookmark_id){
			this.doctorProfile.bookmark_id = null;
		}else{
			this.doctorProfile.bookmark_id = 1;
		}

		this.userProvider.saveBookmarks(thisData).then((data) => {
			if(data['error'] === 0){
				if(thisData['id']){
					this.doctorProfile.bookmark_id = null;
				}else{
					this.doctorProfile.bookmark_id = data['result']['id'];
				}
			}
		}, err => {
			console.log(err);
		});
	}

	seeAllServices(serviceKey, services){
		let doneModal = this.modalCtrl.create(PromptModalPage, {
			message_type: "see_all_services",
			service_type: serviceKey,
			services: services,
		});

		doneModal.onDidDismiss(data => {

		});

		doneModal.present();
	}
}
