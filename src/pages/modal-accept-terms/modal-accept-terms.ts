import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { TabsPage } from '../../pages/tabs/tabs';
import { HmoProvider } from '../../providers/hmo/hmo';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ModalAcceptTermsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-modal-accept-terms',
	templateUrl: 'modal-accept-terms.html',
})
export class ModalAcceptTermsPage {

	action = this.navParams.get("action");
	newHmoAccount = this.navParams.get("newHmoAccount");
	new_sess_user_login = this.navParams.get("new_sess_user_login");
	terms: any;

	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				public hmoProvider: HmoProvider,
				private storage: Storage, 
				public alertCtrl: AlertController, 
				public toastCtrl: ToastController,
				public loadingCtrl: LoadingController, 
				public siteProvider: SiteProvider) {

		this.siteProvider.getCmsPageContent("hmotermsandconditions").then(content => {
			if(content['error'] === 0) {
				this.terms = content['cmspage']['content'];
			} 
		});
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ModalAcceptTermsPage');
	}

	acceptTerms() {
		let loader = this.loadingCtrl.create();
		loader.present();
		console.log(this.newHmoAccount);
		this.hmoProvider.applyNewAccount(this.newHmoAccount).then((result) => {
			loader.dismiss();
			if(result['error'] === 0) {
				console.log("result", result);
				if(this.action === "apply") {
					this.storage.set("sess_user_login", this.new_sess_user_login);
					this.showAlert(result, "Success!", "You've successfully applied new account. Please wait for our confirmation for your application");
				} else {
					this.showAlert(result, "Success!", "You've successfully registered your HMO account. Please wait for our confirmation for validating your account");
				}
			} else {
				this.presentToast(result["message"]);
			}
		}, err => {
			loader.dismiss();
			this.presentToast("Something went wrong. Please try again");
		});
		
	}

	showAlert(result, title, msg) {
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: msg,
			buttons: [{ 
				text: "Okay", 
				handler: () => {
					this.storage.set("sess_user_hmo_account", this.newHmoAccount).then((hmo) => {
						this.navCtrl.setRoot(TabsPage, this.newHmoAccount);
					});
				}
			}]
		});
		alert.present();
	}

	presentToast(msg) {
		this.toastCtrl.create({
			message: msg,
			duration: 2000,
			position: "top"
		}).present();
	}
}
