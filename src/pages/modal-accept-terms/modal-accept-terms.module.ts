import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAcceptTermsPage } from './modal-accept-terms';

@NgModule({
  declarations: [
    ModalAcceptTermsPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAcceptTermsPage),
  ],
})
export class ModalAcceptTermsPageModule {}
