import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalConfirmationAppointmentPage } from './modal-confirmation-appointment';

@NgModule({
  declarations: [
    ModalConfirmationAppointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalConfirmationAppointmentPage),
  ],
})
export class ModalConfirmationAppointmentPageModule {}
