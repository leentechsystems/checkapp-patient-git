import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { TabsPage } from '../../pages/tabs/tabs';

/**
 * Generated class for the ModalConfirmationAppointmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-confirmation-appointment',
  templateUrl: 'modal-confirmation-appointment.html',
})
export class ModalConfirmationAppointmentPage {
  consultation_form = this.navParams.get("consultationForm");
  doctor_profile = this.navParams.get("doctorProfile");
  selected_day = this.navParams.get("selected_day");
  selected_month = this.navParams.get("selected_month");
  selected_year = this.navParams.get("selected_year");
  selected_time = this.navParams.get("selected_time");
  selected_day_of_the_week = this.navParams.get("selected_day_of_the_week");
  monthNames = [];
  dayNames = [];

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams) {
        this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        this.dayNames = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        this.selected_day_of_the_week = this.dayNames[parseInt(this.selected_day_of_the_week) - 1];
        this.selected_month = this.monthNames[parseInt(this.selected_month) - 1];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalConfirmationAppointmentPage');
  }

  escapeModal() {
      this.viewCtrl.dismiss();
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

}
