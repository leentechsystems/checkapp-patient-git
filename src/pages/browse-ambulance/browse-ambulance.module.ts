import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseAmbulancePage } from './browse-ambulance';

@NgModule({
  declarations: [
    BrowseAmbulancePage,
  ],
  imports: [
    IonicPageModule.forChild(BrowseAmbulancePage),
  ],
})
export class BrowseAmbulancePageModule {}
