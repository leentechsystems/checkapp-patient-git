import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HmoProvider } from '../../providers/hmo/hmo';

/**
 * Generated class for the HmoAccountBillDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-hmo-account-bill-details',
	templateUrl: 'hmo-account-bill-details.html',
})
export class HmoAccountBillDetailsPage {
	
	hmo_account = this.navParams.get("hmo_account");
	pageType = this.navParams.get("pageType");
	from_notif = (this.navParams.get('from_notif')) ? this.navParams.get('from_notif') : null;

	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				public hmoProvider: HmoProvider,
				public viewCtrl: ViewController) {
	}

	ionViewDidEnter(){
		console.log(this.hmo_account);
		console.log('pageType',this.pageType);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad HmoAccountBillDetailsPage');
	}

	gobacktoNotif(){
		this.viewCtrl.dismiss();
	}
}
