import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HmoAccountBillDetailsPage } from './hmo-account-bill-details';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    HmoAccountBillDetailsPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(HmoAccountBillDetailsPage),
    IonicImageLoader
  ],
})
export class HmoAccountBillDetailsPageModule {}
