import { Component } from '@angular/core';
import { NavController, ModalController, AlertController, ToastController, Events } from 'ionic-angular';
import { BrowseArticlesPage } from '../../pages/browse-articles/browse-articles';
import { BrowseSpecialtiesPage } from '../../pages/browse-specialties/browse-specialties';
import { ProfileArticlePage } from '../../pages/profile-article/profile-article';
import { AppointmentsCalendarPage } from '../../pages/appointments-calendar/appointments-calendar';
import { BrowseMedicalRecordsPage } from '../../pages/browse-medical-records/browse-medical-records';
import { HmoAccountDetailsPage } from '../../pages/hmo-account-details/hmo-account-details';
import { FormSelectLocationPage } from '../../pages/form-select-location/form-select-location';
import { ModalSelectHmoAccountPage } from '../../pages/modal-select-hmo-account/modal-select-hmo-account';
import { GlobalSearchPage } from '../../pages/global-search/global-search';
import { HmoProvider } from '../../providers/hmo/hmo';
import { SiteProvider } from '../../providers/site/site';
import { Network } from '@ionic-native/network';

import { Storage } from '@ionic/storage';
import { ArticleProvider } from '../../providers/article/article';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	browsearticlespage = BrowseArticlesPage;

	featured_articles: any;
	total_featured_count = 0;
	userid = 0;
	user_name = "";
	hmo_account: any;

	hour: any;
	message = "";
	searched_keyword = "";
	current_page = 1;
	current_location: any;
	no_current_location: any;

	constructor(public navCtrl: NavController, public modalCtrl: ModalController, public articleProvider: ArticleProvider, 
				public hmoProvider: HmoProvider, public siteProvider: SiteProvider, private network: Network,
				private storage: Storage, public alertCtrl: AlertController, public toastCtrl: ToastController, public events: Events) {

		events.subscribe('refresh:mylocation', (locationData, time) => {
			this.storage.get("sess_current_location").then(sess_current_location => {
				if(sess_current_location) {
					this.current_location = sess_current_location;
				} else {
					this.siteProvider.detectCurrentLocation();
				}
			});
		});

		events.subscribe('cant_get_location:mylocation', (data, time) => {
			this.no_current_location = true;
		});

		this.loadFeatureArticles(true);
		this.messageDay();
		this.prepareDefaultData();
	}

	messageDay(){
		let date = new Date();
		this.hour = date.getHours();
		if(this.hour >= 0 && this.hour <=11){
			this.message = "Good morning";

		} else if(this.hour >= 12 && this.hour <=17){
			this.message = "Good afternoon";

		} else{
			this.message = "Good evening";
		}
	}

	prepareDefaultData() {
		this.siteProvider.getRegions();
		this.siteProvider.getCities();
	}

	goToArticleInside(articleProfile) {
		this.navCtrl.push(ProfileArticlePage,{
			articleProfile : articleProfile
		});
	}

	goToSpecialties() {
		this.navCtrl.push(BrowseSpecialtiesPage);
	}

	goToAppointmentsCalendar() {
		this.navCtrl.push(AppointmentsCalendarPage);
	} 

	goToBrowseMedicalRecords() {
		this.navCtrl.push(BrowseMedicalRecordsPage);
	}

	goToHmoAccount() {
		this.loadNewHmoAccount();

		this.storage.get("sess_user_hmo_account").then((hmo) => {
			if(hmo) {
				this.navCtrl.push(HmoAccountDetailsPage, {
					"hmo_account": hmo
				});

			} else {
				this.openHmoAccount();
			}
			
		});
	}


	openHmoAccount() {
		this.modalCtrl.create(ModalSelectHmoAccountPage,{
			'message_type': 'select_hmo_account',
		}).present();
	}

	goToFormSelectLocation() {
		this.navCtrl.push(FormSelectLocationPage);
	}

	gotoGlobalSearch() {
		this.navCtrl.push(GlobalSearchPage,{
			searched_keyword : this.searched_keyword
		});
	}

	loadFeatureArticles(isLoadNew?) {
		this.articleProvider.getFeaturedArticles(isLoadNew).then((data) => {
			if(data['error'] == 0){
				this.featured_articles = data['articles'];
			}
		});
	}

	ionViewDidLoad() {
		this.storage.get('sess_user_login').then((user) => {
			this.userid = user.id;
			this.user_name = user.firstname;
		});
	}

	loadNewHmoAccount() {
		this.storage.get('sess_user_login').then((user) => {
			this.hmoProvider.getUserHmoAccount(true, user).then((hmo) => {
				if(hmo['error'] === 0) {
					this.storage.set("sess_user_hmo_account", hmo['hmo'][0]).then(() => {
						this.hmo_account = hmo['hmo'][0];
					});
				}
			}, err => {
				console.log("No internet connection");
			});
		});
	}

	ionViewDidEnter() {
		this.loadNewHmoAccount();
		this.messageDay();
        /*setTimeout( () => {
          this.siteProvider.detectCurrentLocation();
        }, 5000);*/
		
		this.storage.get("sess_current_location").then(sess_current_location => {
			console.log('sess_current_location', sess_current_location);
			if(sess_current_location) {
				this.current_location = sess_current_location;
			} else {
				this.siteProvider.detectCurrentLocation();
			}
		});

		if(!this.current_location && !this. no_current_location){
			console.log('this.events.publish');
	        setTimeout( () => {
	        	this.events.publish('cant_get_location:mylocation');
	        }, 5000);
		}
	}
}
