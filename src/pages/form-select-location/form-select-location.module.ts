import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormSelectLocationPage } from './form-select-location';

@NgModule({
  declarations: [
    FormSelectLocationPage,
  ],
  imports: [
    IonicPageModule.forChild(FormSelectLocationPage),
  ],
})
export class FormSelectLocationPageModule {}
