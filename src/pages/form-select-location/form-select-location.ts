import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, ModalController, LoadingController, NavParams, Platform, Events, ViewController } from 'ionic-angular';
import { ModalLocationSettingsPage } from '../../pages/modal-location-settings/modal-location-settings';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

declare var google: any;

/**
 * Generated class for the FormSelectLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-select-location',
  templateUrl: 'form-select-location.html',
})
export class FormSelectLocationPage {

  @ViewChild('map') mapElement: ElementRef;

  cities: any;
  lists: any;
  searched_keyword = "";
  current_location: any;
  locationValue: any;
  lat: any;
  lng: any;
  map: any;

  no_result: any;

  service = new google.maps.places.AutocompleteService();

  autocompleteItems: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public siteProvider: SiteProvider, public loadingCtrl: LoadingController, public storage: Storage,
    public platform: Platform, private geolocation: Geolocation, public events: Events, public viewCtrl: ViewController,
    private zone: NgZone, private diagnostic: Diagnostic) {

    this.autocompleteItems = [];

    this.storage.get("sess_cities").then(sess_cities => {
        if(!this.cities) {
          this.cities = [];
        }

        if(sess_cities) {
            sess_cities['city'].forEach((cat, idx) => {
                this.cities.push(cat);
            });
            
        } else {

            let loader = this.loadingCtrl.create();
            loader.present();

            this.siteProvider.getCities().then(cities => {
                loader.dismiss();
                if(cities['error'] === 0) {
                    cities['city'].forEach((cat, idx) => {
                        this.cities.push(cat);
                    });
                }
            }, err => {
                loader.dismiss();
            });
        }
    });

  }

  ionViewDidEnter() {
    this.storage.get("sess_current_location").then(sess_current_location => {
      if(sess_current_location) {
         this.current_location = sess_current_location;
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormSelectLocationPage');
    this.obtenerPosicion();/* Ensure the platform is ready */

    if (this.navParams.get('address') && this.navParams.get('lat') && this.navParams.get('lng')) {
      //this.locationValue = this.navParams.get('address');
      this.lat = this.navParams.get('lat');
      this.lng = this.navParams.get('lng');
    }
    else {
      this.storage.get('sess_current_location').then(data => {
        if (data) {
          //this.locationValue = data;
        }
        console.log("sess_current_location", data);
      });

      this.storage.get('sess_user_loc_coords').then(data => {
        if (data) {
          this.lat = data.lat;
          this.lng = data.lng;
        }
        console.log("sess_user_loc_coords", data);
      });
    }
  }

  obtenerPosicion(){
    console.log('obtenerPosicion');
    
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    this.geolocation.getCurrentPosition(options).then(response => {
      console.log('response',response);
    }).catch(error => {
        console.log('error',error);
    });
  }

  findCurrentLocation() {
    this.obtenerPosicion();
    this.diagnostic.isLocationEnabled().then((state) => {
      console.info("Location state to: " + state);
      if(!state){
        this.modalCtrl.create(ModalLocationSettingsPage,{
          'modal_type' : 'location'
        }).present();
      }else{
        console.log('fdsfd');
        this.siteProvider.detectCurrentLocation();
      }
    });

    this.viewCtrl.dismiss();
  }

  searchCity() {
       this.lists = this.filterItems();
  }

  filterItems(){
      return this.cities.filter((item) => {
          return item.citymunDesc.toLowerCase().indexOf(this.searched_keyword.toLowerCase()) > -1;
      });    
  }

  getValue() {
    if (this.locationValue == '') {
      this.autocompleteItems = [];
      return;
    }

    let me = this;
    this.service.getPlacePredictions({
      input: this.locationValue,
      componentRestrictions: {country: 'ph'},
    }, (predictions, status) => {
      console.log('prediction',predictions);
      me.autocompleteItems = [];
      me.zone.run(() => {
        if (predictions != null) {
          this.no_result = false;
          predictions.forEach((prediction) => {
            me.autocompleteItems.push(prediction.description);
          });

        }else{
          this.no_result = true;
        }
      });
    });
  }

  geoCode(address:any) {
    this.locationValue = address;
    this.autocompleteItems = [];

    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.lat = results[0].geometry.location.lat();
      this.lng = results[0].geometry.location.lng();
      let latLng = new google.maps.LatLng(this.lat, this.lng);
      /*this.map.panTo(latLng);
      this.addMarker();*/
    })

    let array = {
      'address': this.locationValue,
      'lat': this.lat,
      'lng': this.lng
    }

    let coords = {
      lng: this.lng,
      lat: this.lat,
    }
    this.storage.set('sess_user_loc_coords', coords).then(() => {
      console.log('Item Stored');

      console.log(this.lat);
      console.log(this.lng);
      this.storage.get('sess_user_login').then((user) => {
        if(user.address_id){
          let addressData = {
            address_id: user.address_id,
            latitude: this.lat,
            longtitude: this.lng,
          };

          this.siteProvider.updatePatientLocation(addressData).then(res => {
            console.log('addressData', res);
            if(res['error'] == 0){
              user['address_latitude'] = res['data']['latitude'];
              user['address_longtitude'] = res['data']['longtitude'];

              this.storage.set("sess_user_login", user);
            }
          });
        }
      });

    });

    this.storage.set('sess_current_location', this.locationValue).then(() => {
      console.log('Item Stored');
      this.events.publish('refresh:mylocation');
    });

    this.viewCtrl.dismiss();
  }

  hideSearchBar(){
    
  }

 /* getLocationInfo(thisCity) {
      let loader = this.loadingCtrl.create();
      loader.present();

      this.siteProvider.getInfoFromGoogle(thisCity).then(locationinfo => {
          console.log('getLocationInfo',locationinfo);
          loader.dismiss();
          if(locationinfo){
            if(locationinfo['error'] === 0) {
                this.navCtrl.pop();
            } else {
                this.modalCtrl.create(ModalLocationSettingsPage,{
                  'modal_type' : 'location'
                }).present();
            }
          }
      }, err => {
          loader.dismiss();
      });
  }*/
}
