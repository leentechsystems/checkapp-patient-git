import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalLocationSettingsPage } from './modal-location-settings';

@NgModule({
  declarations: [
    ModalLocationSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalLocationSettingsPage),
  ],
})
export class ModalLocationSettingsPageModule {}
