import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileArticlePage } from './profile-article';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    ProfileArticlePage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(ProfileArticlePage),
    IonicImageLoader
  ],
})
export class ProfileArticlePageModule {}
