import { Component } from '@angular/core';
import { IonicPage, NavController, App, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SiteProvider } from '../../providers/site/site';
import { UserProvider } from '../../providers/user/user';

import { BrowseArticlesPage } from './../browse-articles/browse-articles';
import { BrowseDoctorsPage } from './../browse-doctors/browse-doctors';
import { BrowseSpecialtiesPage } from './../browse-specialties/browse-specialties';
import { BrowseHospitalsPage } from './../browse-hospitals/browse-hospitals';
import { BrowseAmbulancePage } from './../browse-ambulance/browse-ambulance';
import { BrowseActivityLogsPage } from './../browse-activity-logs/browse-activity-logs';
import { BrowseHmoTransactionsPage } from './../browse-hmo-transactions/browse-hmo-transactions';
import { ProfileUserPage } from './../profile-user/profile-user';
import { TermsConditionPage } from './../terms-condition/terms-condition';
import { AboutPage } from './../about/about';
import { WalkthroughPage } from './../walkthrough/walkthrough';
import { EmailComposer } from '@ionic-native/email-composer';
import { ContactPage } from './../contact/contact';
import { FaqPage } from './../faq/faq';
import { BookmarkPage } from './../bookmark/bookmark';

/**
 * Generated class for the MorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-more',
	templateUrl: 'more.html',
})
export class MorePage {

	termsConditionPage = TermsConditionPage;
	aboutPage = AboutPage;
	user_profile: any;
	current_location: any;
	photo: any;
	no_current_location: any;

	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				public storage: Storage, 
				private alertCtrl: AlertController, 
				public appCtrl: App, 
				public siteProvider: SiteProvider,
				private emailComposer: EmailComposer, 
				public loadingCtrl: LoadingController, 
				public events: Events,
				public userProvider: UserProvider) {
			
		events.subscribe('refresh:mylocation', (locationData, time) => {
			this.storage.get("sess_current_location").then(sess_current_location => {
				if(sess_current_location) {
					this.current_location = sess_current_location;
				} else {
					this.siteProvider.detectCurrentLocation();
				}
			});
		});

		events.subscribe('cant_get_location:mylocation', (data, time) => {
			console.log('rhan');
			this.no_current_location = true;
		});
	}

	ionViewDidEnter() {
		this.storage.get("sess_current_location").then(currentloc => {
			if(currentloc) {
				this.current_location = currentloc;
			}else {
				this.siteProvider.detectCurrentLocation();
			}
		});

		this.storage.get("sess_user_login").then((val) => {
			this.user_profile = val;
			if(val.attachments_filename && val.attachments_filepath) {
				this.photo = this.siteProvider.base_url + val.attachments_filepath +"/"+ val.attachments_filename;
			}
		});

		if(!this.current_location && !this. no_current_location){
			console.log('this.events.publish');
	        setTimeout( () => {
	        	this.events.publish('cant_get_location:mylocation');
	        }, 5000);
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad MorePage');
	}

	goToBrowseArticles() {
		this.navCtrl.push(BrowseArticlesPage);
	}
	goToBrowseDoctors() {
		this.navCtrl.push(BrowseSpecialtiesPage);
	}
	goToBrowseHospitals() {
		this.navCtrl.push(BrowseHospitalsPage);
	}
	goToBrowseAmbulance() {
		this.navCtrl.push(BrowseAmbulancePage);
	}
	gotoActivityLogs() {
		this.navCtrl.push(BrowseActivityLogsPage);
	}  
	gotoTransactions() {
		this.navCtrl.push(BrowseHmoTransactionsPage);
	}
	gotoProfile() {
		this.navCtrl.push(ProfileUserPage, {
			 "user_profile": this.user_profile
		});
	}
	gotoAbout() {
		this.navCtrl.push(AboutPage);
	}

	gotoBookmark() {
		this.navCtrl.push(BookmarkPage);
	}

	gotoFaq() {
		this.navCtrl.push(FaqPage);
	}

	gotoTerms() {
		this.navCtrl.push(TermsConditionPage);
	}

	gotoContact(){
		this.navCtrl.push(ContactPage);
	}  

	logout() {
		let alert = this.alertCtrl.create({
			title: 'Log Out',
			message: 'Are you sure you want to log out?',
			cssClass: 'AcceptBookingAlert',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Log Out',
					handler: () => {
						let loader = this.loadingCtrl.create();
						loader.present();

						this.storage.get("sess_device_token").then(token => {
							if(token) {
								let thisData = {
									device_token: token
								};

								this.userProvider.removeDeviceToken(thisData).then(data => {
									console.log(data);
								});
							} 
						});

						this.storage.remove('sess_user_login').then(() => {
							this.storage.remove('sess_user_hmo_account').then(() => {
								this.storage.remove('sess_device_token');
								this.appCtrl.getRootNav().setRoot(WalkthroughPage);
								loader.dismiss();
							});
						});
					}
				}
			]
		});
		alert.present();
	}
}