import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MorePage } from './more';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    MorePage,
  ],
  imports: [
    IonicPageModule.forChild(MorePage),
    IonicImageLoader,
  ],
})
export class MorePageModule {}
