import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { ProfileDoctorPage } from '../../pages/profile-doctor/profile-doctor';
import { FormFilterDoctorsPage } from '../../pages/form-filter-doctors/form-filter-doctors';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the BrowseDoctorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-browse-doctors',
	templateUrl: 'browse-doctors.html',
})
export class BrowseDoctorsPage {

	specialties = (this.navParams.get("all_specialties")) ? this.navParams.get("all_specialties") : null;
	selected_specialties = (this.navParams.get("selected_specialties")) ? this.navParams.get("selected_specialties") : [];
	selected_specialties_titles: any = [];
	applied_filters: any;
	current_location: any;

	all_doctors: any;
	current_page = 1;
	total_count = 0;
	hospital_info = (this.navParams.get("hospital")) ? this.navParams.get("hospital") : null;

	searched_keyword = (this.navParams.get('searched_keyword')) ? this.navParams.get('searched_keyword') : "";

	constructor(public navCtrl: NavController, 
				public storage: Storage,
				public navParams: NavParams,
				public modalCtrl: ModalController,
				public doctorProvider: DoctorProvider,
				public userProvider: UserProvider) {

		if(this.selected_specialties){
			this.selected_specialties.forEach((cat, idx) => {
				if(cat.title !== undefined) {
					this.selected_specialties_titles.push(cat.title)
				}
			});
		} 

		this.storage.get("sess_current_location").then(sess_current_location => {
			if(sess_current_location) {
				this.current_location = sess_current_location;
			}
			this.loadAllData(false, true, false);
		}); 
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad BrowseDoctorsPage');
	}
	
	reloadData(refresher?){
		this.all_doctors = null;
		this.current_page = 1;
		this.loadAllData(refresher, true, false);
	}

	goToDoctorProfile(doctorProfile) {
		this.navCtrl.push(ProfileDoctorPage,{
			doctorProfile : doctorProfile
		});
	}

	openFilterForm() {
		let filtermodal = this.modalCtrl.create(FormFilterDoctorsPage, {
			"specialties": this.specialties
		});

		filtermodal.onDidDismiss(data => { 
			if(data) {
				this.applied_filters = data;
				if(this.applied_filters.city) {
					this.all_doctors = null;
					this.current_page = 1;
				}
				this.loadAllData(false,true,false);
			}
		});

		filtermodal.present();
	}

	loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
		
		console.log(this.current_page);
			let hospital_id = "";
			if(this.hospital_info) {
					hospital_id = this.hospital_info.id
			}

			let origin_coordinates = {};

			if(this.current_location) {
				origin_coordinates = {
					"lat": this.current_location['lat'],
					"lng": this.current_location['lng'],
				};
			} else {
				origin_coordinates = {
					"lat": 0,
					"lng": 0,
				};
			}

			let thisData = {
				"current_page": this.current_page,
				"searched_keyword": this.searched_keyword,
				"applied_filters": this.applied_filters,
				"selected_specialties": this.selected_specialties_titles,
				"hospital": hospital_id,
				"origin_coordinates": origin_coordinates
			};

			 this.doctorProvider.getAllDoctors(isLoadNew, thisData).then(data => {
					console.log(data);
					if(data['error'] == 0){
						this.total_count = data['result']['total_count'];
						
						if(!infiniteScroll && infiniteScroll === false) {
							if(!this.all_doctors || isLoadNew === true) {
									 this.all_doctors = []; 
								}
						}

						data['result']['items'].forEach((cat, idx) => {
									if(cat.id !== undefined) {
											this.all_doctors.push(cat);
									}
						});
						
						if(infiniteScroll) {
							infiniteScroll.complete();
						}
					}

					if(refresher) {
						refresher.complete();
					}
		});
	}

	doInfinite(infiniteScroll:any) {
		this.current_page += 1;
		console.log('infiniteScroll',infiniteScroll);
		if(this.all_doctors.length < this.total_count) {
			this.loadAllData(false, false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
	}

	searchNews(onCancel:any) {
		if(onCancel) {
			this.searched_keyword = "";
		}
		this.all_doctors = null;  
		this.current_page = 1; 
		this.loadAllData(false, true, false); 
	} 

	addtoBookmark(temp_doctor, doctor_index){
		console.log('doctor',temp_doctor);
		console.log('index',doctor_index);

		let thisData = {
			"item_id": temp_doctor.user_id,
			"type": 'doctor',
		};

		if(temp_doctor.bookmark_id){
			thisData['id'] = temp_doctor.bookmark_id;
		}

		this.all_doctors.forEach((cat, idx) => {
			if(doctor_index == idx){
				if(temp_doctor.bookmark_id){
					cat.bookmark_id = null;
				}else{
					cat.bookmark_id = 1;
				}
			}
		});

		this.userProvider.saveBookmarks(thisData).then((data) => {
			if(data['error'] === 0){
				this.all_doctors.forEach((cat, idx) => {
					if(doctor_index == idx){
						if(thisData['id']){
							cat.bookmark_id = null;
						}else{
							cat.bookmark_id = data['result']['id'];
						}
					}
				});
			}
		}, err => {
			console.log(err);
		});
	}
}
