import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ProfileDoctorPage } from '../../pages/profile-doctor/profile-doctor';
import { BrowseDoctorsPage } from '../../pages/browse-doctors/browse-doctors';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { GoogleMap, GoogleMaps } from '@ionic-native/google-maps'; 
import { SiteProvider } from '../../providers/site/site';
import { UserProvider } from '../../providers/user/user';
import { PromptModalPage } from '../../pages/prompt-modal/prompt-modal';

/**
 * Generated class for the ProfileHospitalsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 declare var google;

@IonicPage()
@Component({
  selector: 'page-profile-hospitals',
  templateUrl: 'profile-hospitals.html',
})
export class ProfileHospitalsPage {
  public hospital_tabs = "overview";
  profile = this.navParams.get('hospital_profile');
  doctors: any;
  doctor_schedules: any;
  hospital_location: any;
  marker: any;
  hospital_services: any;
  total_hospital_services = 0;
  selected_service: any;
  total_count = 0;

  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;

  constructor(public navCtrl: NavController, public navParams: NavParams, public hospitalsProvider: HospitalsProvider,
    public doctorProvider: DoctorProvider, private googleMaps: GoogleMaps, public siteProvider: SiteProvider,
    public modalCtrl: ModalController, public userProvider: UserProvider) {
    
    this.loadDoctors(true);
    console.log(this.profile);

    if(this.profile.description) {
      this.hospital_tabs = "overview";
    } else {
      this.hospital_tabs = "services";
    }

    this.getHospitalServices();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileHospitalsPage');
    if(this.profile.hospital_latitude && this.profile.hospital_longtitude) {
       this.initializeMap();
    }
  }

  loadDoctors(isLoadNew) {
      let thisData = {
          "id": this.profile.id
      }
      this.hospitalsProvider.getAllHospitalDoctors(isLoadNew, thisData).then((res) => {
          if(res['hospital']) {
              this.doctors = res['hospital'];
          }
      });
  }

  gotoDoctorProfile(thisDoctor) {
    // thisDoctor['prefix'] = thisDoctor['doctor_prefix'];
    // thisDoctor['suffix'] = thisDoctor['doctor_suffix'];
    // thisDoctor['firstname'] = thisDoctor['doctor_firstname'];
    // thisDoctor['lastname'] = thisDoctor['doctor_lastname'];
    // thisDoctor['medical_degree'] = thisDoctor['doctor_medical_degree'];
    // thisDoctor['hospital_name'] = this.profile['name'];
    // thisDoctor['hospital_address'] = this.profile['hospital_address'];
    // thisDoctor['hospital_region'] = this.profile['hospital_region'];
    // thisDoctor['hospital_country'] = this.profile['hospital_country'];
    // thisDoctor['hospital_zip'] = this.profile['hospital_zip'];
    // thisDoctor['hospital_latitude'] = this.profile['hospital_latitude'];
    // thisDoctor['hospital_longtitude'] = this.profile['hospital_longtitude'];
    // thisDoctor['hospital_doctors_main'] = thisDoctor['main'];
    // thisDoctor['hospital_doctors_hospital_id'] = this.profile['id'];
    // thisDoctor['status'] = "1";
    // thisDoctor['id'] = thisDoctor['user_id'];
 
    this.navCtrl.push(ProfileDoctorPage, {
      "doctorProfile": thisDoctor
    });
  }

  goToBrowseDoctors(thisId) {
      this.navCtrl.push(BrowseDoctorsPage, {
        "hospital": this.profile
      });
  }


  initializeMap() {
      let element = this.mapElement.nativeElement;
      // this.map = this.googleMaps.create(element);
      this.map = new google.maps.Map(this.mapElement.nativeElement, {
          zoom: 13,
        center: {lat: parseFloat(this.profile['hospital_latitude']), lng: parseFloat(this.profile['hospital_longtitude'])},
        mapTypeControl: false,
        streetViewControl: false
      });

      this.marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: {lat: parseFloat(this.profile['hospital_latitude']), lng: parseFloat(this.profile['hospital_longtitude'])},
      });
  }

  getHospitalServices() {
    this.hospitalsProvider.getServices(this.profile.id).then(services => {
       if(services['error'] === 0) { 
          this.total_hospital_services = services['services']['service_count'];
          this.hospital_services = services['services']['data'];
          this.total_count = services['services']['total_count'];
          console.log(this.hospital_services);
       }
    }, err => {
        console.log("getServices", err);
    });
  }

  addtoBookmark(temp_bookmark, temp_id, temp_action, bookmark_index=null){
    console.log('temp_bookmark',temp_bookmark);
    console.log('temp_action',temp_action);
    console.log('bookmark_index',bookmark_index);

    let thisData = {
      "item_id": temp_id,
      "type": temp_action,
    };

    if(temp_bookmark.bookmark_id){
      thisData['id'] = temp_bookmark.bookmark_id;
    }

    if(temp_bookmark.bookmark_id){
      if(bookmark_index || bookmark_index == 0){
        this.doctors.forEach((cat, idx) => {
          if(bookmark_index == idx){
            cat.bookmark_id = null;
          }
        });
      }else{
        this.profile.bookmark_id = null;
      }
    }else{
      if(bookmark_index || bookmark_index == 0){
        this.doctors.forEach((cat, idx) => {
          if(bookmark_index == idx){
            cat.bookmark_id = 1;
          }
        });
      }else{
        this.profile.bookmark_id = 1;
      }
    }

    this.userProvider.saveBookmarks(thisData).then((data) => {

      if(data['error'] === 0){
        if(thisData['id']){
          if(bookmark_index || bookmark_index == 0){
            this.doctors.forEach((cat, idx) => {
              if(bookmark_index == idx){
                cat.bookmark_id = null;
              }
            });
          }else{
            this.profile.bookmark_id = null;
          }
        }else{
          if(bookmark_index || bookmark_index == 0){
            this.doctors.forEach((cat, idx) => {
              if(bookmark_index == idx){
                cat.bookmark_id = data['result']['id'];
              }
            });
          }else{
            this.profile.bookmark_id = data['result']['id'];
          }
        }
      }

    }, err => {

      console.log(err);

    });
  }

  seeAllServices(serviceKey, services){
      let doneModal = this.modalCtrl.create(PromptModalPage, {
            message_type: "see_all_services",
            service_type: serviceKey,
            services: services,
        });
      doneModal.onDidDismiss(data => {

      });

      doneModal.present();
  }
}

