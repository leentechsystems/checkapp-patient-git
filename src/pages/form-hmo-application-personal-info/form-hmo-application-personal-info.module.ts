import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormHmoApplicationPersonalInfoPage } from './form-hmo-application-personal-info';

@NgModule({
  declarations: [
    FormHmoApplicationPersonalInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(FormHmoApplicationPersonalInfoPage),
  ],
})
export class FormHmoApplicationPersonalInfoPageModule {}
