import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { SiteProvider } from '../../providers/site/site';

import { ModalAcceptTermsPage } from './../modal-accept-terms/modal-accept-terms';

/**
 * Generated class for the FormHmoApplicationPersonalInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-hmo-application-personal-info',
  templateUrl: 'form-hmo-application-personal-info.html',
})
export class FormHmoApplicationPersonalInfoPage {
  
  	@ViewChild(Slides) slides: Slides;
  	action = this.navParams.get("action");
  	btn_label = "Next";
  	currentIndex = 0;
  	public newHmoAccount : FormGroup;
  	public step2Form: FormGroup;
  	public step3Form: FormGroup;
  	public existingHmoAccount: FormGroup;
  	user_profile: any;
  	is_submit_add = false;
  	is_submit_step = false;
  	is_submit_step1 = false;
  	is_submit_step2 = false;
  	is_submit_step3 = false;

  	total_zip_code = 0;
  	zipcode: any;
  	total_company_zip_code = 0;
  	company_zipcode: any;
  	cities: any;
  	regions: any;
  	company_cities: any;

  	is_change_region = false;
  	is_change_company_region = false;
  	is_change_city = false;
  	is_change_company_city = false;

  	company_city : any;
  	company_region: any;
  	company_zip: any;

  	city : any;
  	region: any;
  	zip: any;

  	contact: any;

  	civil_status = "single";
  	contact_type = "home";

  	selectOptions = {
		title: 'City',
		subTitle: 'Please select a province first.',
		mode: 'md'
  	};

  	selectZipcodeOptions = {
		title: 'Zipcode',
		subTitle: 'Please select a city first.',
		mode: 'md'
  	};
  	
  	hide_area_code = false;
  	formErrorNumber = 'Mobile Number must be at least 10 digit';
  	focus_number = false;

  	hmo_account = (this.navParams.get('hmo_account')) ? this.navParams.get('hmo_account') : null;
  	hmo_provider = (this.navParams.get('hmo_provider')) ? this.navParams.get('hmo_provider') : null;
  	hmo_membership_plan = (this.navParams.get('hmo_membership_plan')) ? this.navParams.get('hmo_membership_plan') : null;

  	constructor(public navCtrl: NavController, 
  				public navParams: NavParams, 
  				public formBuilder: FormBuilder,
				private storage: Storage, 
				private loadingCtrl: LoadingController, 
				public toastCtrl: ToastController,
				public siteProvider: SiteProvider) {

	  	console.log('hmo_account', this.hmo_account);
	  	let loader = this.loadingCtrl.create();
	  	loader.present();

	  	this.storage.get("sess_user_login").then((user) => {
	  		if(user){
			  	this.user_profile = user;
			  	if(user.contact){
			  		this.contact = user.contact;
			  	}
			  	console.log('this.user_profile',this.user_profile);
			  	console.log('this.contact',this.contact);
			  	loader.dismiss();
	  		}
	  	});

	  	this.storage.get("sess_regions").then(regions => {
		  	this.regions = regions;
	  	});

	  	/* Existing HMO Account */
	  	this.existingHmoAccount = formBuilder.group({
		  	account_number: ['', Validators.compose([Validators.pattern('^[A-Za-z0-9? ]+$'), Validators.required])],
		  	date_of_birth: ['', Validators.required],

		  	hmo_membership_id : [this.hmo_membership_plan.id],
		  	provider_id: [this.hmo_provider.id],
		  	firstname : [this.user_profile ? this.user_profile.firstname : ''],
		  	midlename: [this.user_profile ? this.user_profile.midlename : ''],
		  	mothers_maiden_name: [this.user_profile ? this.user_profile.mothers_maiden_name : ''],
		  	lastname: [this.user_profile ? this.user_profile.lastname : ''],
		  	suffix: [this.user_profile ? this.user_profile.suffix : ''],
		  	email: [this.user_profile ? this.user_profile.email : ''],
	  	});

	  	/* New Apply HMO Account*/
	  	this.newHmoAccount = formBuilder.group({
			hmo_membership_id : [this.hmo_membership_plan.id, Validators.required],
			provider_id: [this.hmo_provider.id, Validators.required],
			firstname : ['', Validators.required],
			midlename: [''],
			mothers_maiden_name: [''],
			lastname: ['', Validators.required],
			suffix: [''],
			email: ['', Validators.required],
			date_of_birth: ['', Validators.required],
			place_of_birth: ['', Validators.required],
			height: [''],
			weight: [''],
			civil_status: [this.civil_status],

			company_name: [''],
			company_id_no: [''],
			company_job_title: [''],
			tin_no: [''],

			/* Company Address */
			company_country: ['PH'],
			company_address: [''],
			company_longtitude: [''],
			company_latitude: [''],
			/* Address */
			country: ['PH', Validators.required],
			address: ['', Validators.required],
			longtitude: [''],
			latitude: [''],
			/* Contact Info */
			country_code: ['+63'],
			area_code: ['+63', Validators.compose([Validators.minLength(4), Validators.maxLength(4)])],
			number: ['', Validators.required],
			type: [this.contact_type, Validators.required],
	  	});
  	}

  	ionViewDidLoad() {
		console.log('ionViewDidLoad FormHmoApplicationPersonalInfoPage');
  	}

  	nextSlide(thisStep) {
		this.newHmoAccount.value['id'] = this.user_profile.id;
		this.existingHmoAccount.value['id'] = this.user_profile.id;
   
		if(this.action === "apply") {
			if(this.slides.isEnd()) {
			  this.is_submit_step1 = true;

			  	if(!this.newHmoAccount.valid || ((!this.city || !this.region || !this.zip) || (this.city === "" || this.region === "" || this.zip === ""))) {
				 	this.presentToast("Please fill-up all required fields");
			  	} else {
					console.log('this.newHmoAccount',this.newHmoAccount);

					let thisData = this.newHmoAccount.value;
					thisData['company_region'] = this.company_region ? this.company_region['provDesc'] : null;
					thisData['company_city'] = this.company_city != undefined ? this.company_city : null;
					thisData['company_zip'] = this.company_zip != undefined ? this.company_zip : null;

					thisData['region'] = this.region['provDesc'];
					thisData['city'] = this.city;
					thisData['zip'] = this.zip;
					thisData['id'] = this.hmo_account ? this.hmo_account.id : null;

					thisData['contact'] = [{
					  	'number' : this.newHmoAccount.value['number'],
					  	'type' : this.contact_type,
					  	'country_code' : this.newHmoAccount.value['country_code'],
					  	'area_code' : this.newHmoAccount.value['area_code'],
					}];
					thisData['address_id'] = this.user_profile['address_id'];

					// save to local storage
					this.contact = thisData['contact'];

					let new_sess_user_login = this.user_profile;
					new_sess_user_login['contact'] = this.contact;
					new_sess_user_login['prefix'] = this.newHmoAccount.value['prefix'];
					new_sess_user_login['firstname'] = this.newHmoAccount.value['firstname'];
					new_sess_user_login['midlename'] = this.newHmoAccount.value['midlename'];
					new_sess_user_login['mothers_maiden_name'] = this.newHmoAccount.value['mothers_maiden_name'];
					new_sess_user_login['lastname'] = this.newHmoAccount.value['lastname'];
					new_sess_user_login['suffix'] = this.newHmoAccount.value['suffix'];
					new_sess_user_login['date_of_birth'] = this.newHmoAccount.value['date_of_birth'];
					new_sess_user_login['place_of_birth'] = this.newHmoAccount.value['place_of_birth'];
					new_sess_user_login['height'] = this.newHmoAccount.value['height'];
					new_sess_user_login['weight'] = this.newHmoAccount.value['weight'];
					new_sess_user_login['civil_status'] = this.newHmoAccount.value['civil_status'];
					new_sess_user_login['address_country'] = this.newHmoAccount.value['country'];
					new_sess_user_login['address_region'] = this.region['provDesc'];
					new_sess_user_login['address_city'] = this.city;
					new_sess_user_login['address_address'] = this.newHmoAccount.value['address'];
					new_sess_user_login['address_zip'] = this.zip;

					//end of save to local storage
					console.log('thisData', thisData);
					this.navCtrl.push(ModalAcceptTermsPage, {
						"action": this.action,
						"newHmoAccount": thisData,
						"new_sess_user_login": new_sess_user_login
					});
			  	}
			} else {
			  	if(thisStep == 'newHmoAccount'){
					console.log(this.newHmoAccount);
					if(this.newHmoAccount.value['date_of_birth'] == '' || this.newHmoAccount.value['place_of_birth'] == ''){
				  		this.is_submit_step = true;
				  		this.presentToast("Please fill-up all required fields");
					}else{
				  		this.slides.slideNext(); 
					}
			  	}else{
					this.slides.slideNext(); 
			  	}
			}
		} else {
	  		this.is_submit_add = true;

		  	if(!this.existingHmoAccount.valid) {
			  	this.presentToast("Please fill-up all required fields");
			  
		  	} else {

				let thisData = this.existingHmoAccount.value;
				thisData['region'] = "";
				thisData['city'] = "";
				thisData['id'] = this.hmo_account ? this.hmo_account.id : null;

				console.log(thisData); 

				this.navCtrl.push(ModalAcceptTermsPage, {
					"action": this.action,
					"newHmoAccount": thisData
				});
			}
  		}
  	}

  	prevSlide() {
		if(this.action === "apply") {
			if(this.slides.isBeginning() && this.currentIndex === 0) {
				this.navCtrl.pop();
			} else {
				this.slides.slidePrev();
			} 
		} else {
		  this.navCtrl.pop();
		}
  	}

  	presentToast(msg) {
	  	this.toastCtrl.create({
		  	message: msg,
		  	duration: 2000,
		  	position: "top"
	  	}).present();
 	}

  	getCity(company=null) {
	  	let temp_region;

	  	if(company){
			this.is_change_company_region = true;
			temp_region = this.company_region['provCode'];
	  	}else{
			this.is_change_region = true;
			temp_region = this.region['provCode'];
	  	}

	  	this.siteProvider.getCitiesByRegions(temp_region).then(city => {
			if(city['error'] == 0){
			  	if(company){
					this.company_cities = city['city'];
					this.is_change_company_region = false;

					this.total_company_zip_code = 0;
					this.company_zipcode = null;
			  	}else{
					this.cities = city['city'];
					this.is_change_region = false;

					this.total_zip_code = 0;
					this.zipcode = null;
			  	}
			}
	  	});
  	}

  	getZipcode(company=null) {
		let temp_city;

		if(company){
		  	this.is_change_company_city = true;
		  	temp_city = this.company_city;
		}else{
		  	this.is_change_city = true;
		  	temp_city = this.city;
		}

		this.siteProvider.getZipcodeByCity(temp_city).then(data => {
			if(data['error'] == 0){
			  	if(company){
					this.total_company_zip_code = data['total_count'];
					this.company_zipcode = data['zipcode'];
					this.is_change_company_city = false;
			  	}else{
					this.total_zip_code = data['total_count'];
					this.zipcode = data['zipcode'];
					this.is_change_city = false;
			  	}
			}
		});
  	}

  	contactTypeChange(){
  		console.log(this.contact_type);
		if(this.contact_type== 'mobile'){
		  	this.newHmoAccount.controls.number.setValidators([Validators.required, Validators.minLength(10), Validators.maxLength(10)]);
		  	this.newHmoAccount.controls.area_code.setValidators([]);
	  		this.newHmoAccount.controls['area_code'].setValue('');
		  	this.formErrorNumber = 'Mobile Number must be at least 10 digit only';
		  	this.hide_area_code = true;

		}else{
		  	this.newHmoAccount.controls.area_code.setValidators([Validators.required, Validators.minLength(4), Validators.maxLength(4)]);
		  	this.newHmoAccount.controls.number.setValidators([Validators.required, Validators.minLength(7), Validators.maxLength(7)]);
		  	this.formErrorNumber = 'Mobile Number must be at least 7 digit only';
			this.hide_area_code = false;
		}
  	}

  	checkFocus(){
  		this.focus_number = true;
  	}

  	checkBlur(){
  		this.focus_number = false;
  	}
}
