import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormHmoPaymentPage } from './form-hmo-payment';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    FormHmoPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(FormHmoPaymentPage),
    IonicImageLoader
  ],
})
export class FormHmoPaymentPageModule {}
