import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, ActionSheetController, ModalController, AlertController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera } from '@ionic-native/camera';
import { normalizeURL } from 'ionic-angular';
import { ModalSelectHmoAccountPage} from '../../pages/modal-select-hmo-account/modal-select-hmo-account';
import { HmoProvider } from '../../providers/hmo/hmo';
import { SiteProvider } from '../../providers/site/site';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/timeout';

/**
 * Generated class for the FormHmoPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-form-hmo-payment',
	templateUrl: 'form-hmo-payment.html',
})
export class FormHmoPaymentPage {

  	public paymentForm: FormGroup;
  	is_submit = false;
  	photos: any;
  	hmo_account = this.navParams.get("hmo_account");

	today: any;
  	today_year: any;
  	today_month: any;
  	today_day: any;
  	today_hours: any;
  	today_minute: any;
  	//today_second: any;
  	current_date: any;
  	current_time: any;
  	public event: any;
  	payment_amount: any;
  	payment_method = (this.navParams.get("payment_method")) ? this.navParams.get("payment_method") : null;
 
	constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController,
    			public imagePicker: ImagePicker, private camera: Camera, public formBuilder: FormBuilder, 
    			public toastCtrl: ToastController, public loadingCtrl: LoadingController, public modalCtrl: ModalController,
    			public hmoProvider: HmoProvider, public siteProvider: SiteProvider, private alertCtrl: AlertController,
    			public events: Events) { 
		console.log(this.hmo_account);

 		let temp_payment = this.hmo_account.plan_price.split(".");
        this.payment_amount = temp_payment[0];
    //     if(this.payment_method){
 			// let temp_method = this.payment_method.config.split(":");
 			// let temp_method1 = temp_method[1].split(",");
 			// let temp_account_name = temp_method[2].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');
    //     	this.payment_method['account_name'] = temp_account_name;
    //     	this.payment_method['account_number'] = temp_method1[0];
    //     }

	    this.today = new Date();
      	this.today_month = this.today.getMonth() + 1;
      	this.today_day = this.today.getDate();
      	this.today_year = this.today.getFullYear();
      	this.today_hours = this.today.getHours();
      	this.today_minute = this.today.getMinutes();
      	//this.today_second = this.today.getSeconds();

      	if(this.today_month < 10){
        	this.today_month = '0'+this.today_month;
      	}
      	if(this.today_day < 10){
        	this.today_day = '0'+this.today_day;
      	}
      	if(this.today_hours < 10){
        	this.today_hours = '0'+this.today_hours;
      	}
      	if(this.today_minute < 10){
        	this.today_minute = '0'+this.today_minute;
      	}

	    this.current_date = this.today_year +"-"+ this.today_month+"-"+this.today_day;
	    this.current_time = this.today_hours +":"+ this.today_minute;

		this.event = {
			date: this.current_date,
			time: this.current_time, //07:23
		};

		this.paymentForm = formBuilder.group({
			hmo_account_id: ['', Validators.required],
			payment_amount: ['', Validators.required],
			hmo_account_name: ['', Validators.required],
			hmo_account_email: ['', Validators.required],
			payment_date: ['', Validators.required],
			payment_time: ['', Validators.required],
			payment_remarks: [''],
    	});
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad FormHmoPaymentPage');
	}  	

	goToPaymentFormStep2() {

		console.log(this.photos);
		this.is_submit = true;

		if(this.paymentForm.valid && this.photos) {
			this.hmo_account['date_paid'] = this.paymentForm.value.payment_date +" "+this.paymentForm.value.payment_time+":00";
			
			let wordDate1 = this.paymentForm.value.payment_time.split(":");

                if(wordDate1[0] < 12){
                  	this.hmo_account['time_paid'] = wordDate1[0]+':'+wordDate1[1]+' AM';
                }else{

                  	if(wordDate1[0] == 12){
                    	this.hmo_account['time_paid'] = '12'+':'+wordDate1[1]+' PM';

                  	}else{
                    	this.hmo_account['time_paid'] = (wordDate1[0]-12) +':'+wordDate1[1]+' PM';
                  	}
                }

	        let paymentModal = this.modalCtrl.create(ModalSelectHmoAccountPage,{
	        	'message_type': 'payment',
				'hmo_account': this.hmo_account,
				'paymentForm': this.paymentForm.value,
				'paymentMethod': this.payment_method,
				'photos': this.photos,
	        });

	        paymentModal.onDidDismiss(data => { 

	            if(data){
	            	let paymentDetails = {
	            		'amount_paid' : this.payment_amount,
	            		'amountreceived' : this.payment_amount,
	            		'date_paid' : this.paymentForm.value.payment_date +" "+this.paymentForm.value.payment_time+":00", //"2018-05-29 00:00:00",
	            		'hmo_account_id' : this.hmo_account.id,
	            		'payment_gateway_id' : '1',
	            		'status' : '2',
	            	};

					let loader = this.loadingCtrl.create({
				       	content: 'Saving...'
				    });
          			loader.present(); 

				    this.hmoProvider.uploadPaymentSlip(paymentDetails).then((result) => {
				        console.log("result", result);
				         if(result['error'] === 0) {      
	            			loader.dismiss(); 
	            			if(this.photos){
				            	this.sendDataSkip(result); 
	            			}      
				    	} else {
			          		loader.dismiss();
				            this.presentToast("Something went wrong uploading files please try again.");
				            /* Finish this process */
				        }
				    }, err => {
				        this.presentToast("Something went wrong. Please try again");
				    });
	            }
	        });
	        paymentModal.present();

	 	} else {

			this.presentToast("Please upload your payment slip");
	 	}
  	}

    sendDataSkip(temp_result) {
        if(this.photos) {

	      	let loader = this.loadingCtrl.create({
	        	content: 'Uploading Attachment...'
	      	});

	      	loader.present();
	        Observable.forkJoin([
			this.siteProvider.uploadPhoto(this.photos)]).subscribe(res => {
				console.log('res',res);
		        if(res[0]['error'] === 0) {      
		            loader.dismiss(); 
		            this.gotoHomeSkip(temp_result, res);         
		        /* If moving of files fails*/     
		    	} else {
	          		loader.dismiss();
		            this.presentToast("Something went wrong uploading files please try again.");
		            /* Finish this process */
		        }
		    });

		}else{

		    this.showAlert(this.hmo_account, "Success!", "You've successfully paid "+ this.payment_amount +" pesos to "+ this.hmo_account['provider_name'] +". Please wait for our approval for validating your payment");
		}
    }

    gotoHomeSkip(temp_result, res){
      	let attachments = [];
	   	res[0]['items'].forEach((items, iidx) => {
	       	attachments.push({"filename": items.files[0].filename, "filepath": items.files[0].filepath})
	    });

	    let attachmentsData = {
	        "attachment_type": "payment_history",
	        "item_id": temp_result['result']['paymentData']['payment_id'],
	        "attachment": attachments
	    };

      	let loader = this.loadingCtrl.create({
        	content: 'Sending data...'
      	});

      	loader.present();
		Observable.forkJoin([
	    this.siteProvider.saveAttachment(attachmentsData)]).subscribe(saveAttachmentResult => {
	    	console.log('saveAttachmentResult',saveAttachmentResult);
	        if(saveAttachmentResult[0]['error'] === 0) {
	            loader.dismiss();
	            console.log("saveAttachmentResult", saveAttachmentResult);
	           	this.showAlert(temp_result, "Success!", "You've successfully paid "+ this.payment_amount +" pesos to "+ this.hmo_account['provider_name'] +". Please wait for our approval for validating your payment");			
	        } else {
	            loader.dismiss();
	            this.presentToast("Something went wrong uploading files please try again.");
	       }
	   	});
    }

  	showAlert(result, title, msg) {
    	let res = [];
    	res['hmo'] = result;

    	let alert = this.alertCtrl.create({
     	 	title: title,
      		subTitle: msg,
        	buttons: [{ 
          		text: "Okay", 
          		handler: () => {
          			this.events.publish('hmo_payment:created', 'true', Date.now());
              		this.navCtrl.pop();
          		}
        	}]
    	});
    	alert.present();
  	}

	presentActionSheet() {
	    let actionSheet = this.actionSheetCtrl.create({
	        title: 'Select Image Source',
	        buttons: [{
	            text: 'Load from Library',
	            handler: () => { this.openImagePicker(); }
	        },
	        {
	            text: 'Use Camera',
	            handler: () => { this.takePicture(); }
	        },
	        {
	           	text: 'Cancel',
	            role: 'cancel'
	        }]
	    });
	    actionSheet.present();
	}

   	openImagePicker(){
	  	if(!this.photos) {
			this.photos = new Array<string>();
	  	}

	  	if(this.photos.length < 1) {
		  	let options= {
				maximumImagesCount: 1,
				quality: 70,
		  	}

		  	this.imagePicker.getPictures(options).then((results) => {
			  	if(results !== 'OK'){
				  	results.forEach((pp, pidx) => {
						let pp_pieces = pp.split("/");
						let new_pp = {
					  		"name": pp_pieces[pp_pieces.length - 1],
					  		"path": normalizeURL(pp)
						};
					this.photos.push(new_pp);
					//this.photos = new_pp;
				  	});
				}
		  	}, (err) => { console.log(err) });
	 	} else {
			this.presentToast("You can upload one (1) photo only");
	 	}
  	}

  	takePicture(){
	  	if(!this.photos) {
			this.photos = new Array<string>();
	  	}

	  	if(this.photos.length < 1) {
		  	let options = {
				quality: 70,
				correctOrientation: true
		  	};

		  	this.camera.getPicture(options).then((data) => {
			  	let pp_pieces = data.split("/");
			  	let new_pp = {
					"name": pp_pieces[pp_pieces.length - 1],
					"path": normalizeURL(data)
			  	};
			  	this.photos.push(new_pp);
				//this.photos = new_pp;
		  	}, function(error) {
			 	console.log(error);
		  	});
	  	} else {
			this.presentToast("You can upload one (1) photo only");
	  	}
  	}

  	removePhoto(thisIdx) {
	  	this.photos.splice(thisIdx, 1);
	  	this.photos = null;
  	}

  	presentToast(msg) {
      	this.toastCtrl.create({
         	message: msg,
          	duration: 2000,
          	position: "top"
      	}).present();
  	}
}
