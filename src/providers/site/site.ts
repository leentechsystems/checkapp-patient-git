import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable, NgZone } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { Storage } from '@ionic/storage';
import {JwtHelper} from "angular2-jwt";
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
import 'rxjs/add/operator/filter';
import { Network } from '@ionic-native/network';
import { ToastController, ModalController, Platform, Events } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { ModalLocationSettingsPage } from '../../pages/modal-location-settings/modal-location-settings';
import { AppVersion } from '@ionic-native/app-version';

/*
	Generated class for the SiteProvider provider.

	See https://angular.io/guide/dependency-injection for more info on providers
	and Angular DI.
*/
@Injectable()
export class SiteProvider {
	watch: any;
	jwtHelper = new JwtHelper();

	/* For Local */
	// public protocol = "http";
    // public base_url = "http://checkapp.local";
    // data_url = "http://checkapp.local/api";
    // public url = "http://checkapp.local/";

	/* For Development */
	public protocol = "https";
	public base_url = "https://dev.checkapptech.com";
	data_url = "https://dev.checkapptech.com/api";
	public url = "https://dev.checkapptech.com/";

	/* For Testing */
	/*public protocol = "https";
	public base_url = "https://test.checkapptech.com";
	data_url = "https://test.checkapptech.com/api";
	public url = "https://test.checkapptech.com/";*/

	/*http://dev.checkapptech.com/cgi-sys/suspendedpage.cgi*/

	/* For Production */
	// public protocol = "https";
	// public base_url = "https://checkapptech.com";
	// data_url = "https://checkapptech.com/api";
	// public url = "https://checkapptech.com/";

	public lat: number = 0;
	public lng: number = 0;
	public coords: any = {
		lng: 0,
		lat: 0,
	}
	public current_location: any;
	public network_status: any;
	public generic_error_msg = "Something went wrong. Please try again.";
	ttl = 60 * 60 * 3;

	timeout = 10000;
	toast_counter = 0;

	public isApp;

	constructor(public http: Http,
				private cache: CacheService,
				private geolocation: Geolocation,
				public zone: NgZone,
				public storage: Storage,
				private backgroundGeolocation: BackgroundGeolocation,
				private imageResizer: ImageResizer,
				private transfer: Transfer,
				private network: Network,
				public toastCtrl: ToastController,
				private file: File,
				public events: Events,
				public nativeGeocoder: NativeGeocoder,
				public modalCtrl: ModalController,
				public platform: Platform,
				public appVersion: AppVersion) {

		this.storage.get("sess_user_token").then((token) => {
			if(token) {
				// Decode the Token
				let decodedToken = this.jwtHelper.decodeToken(token);
				this.base_url = this.protocol +"://"+ decodedToken['iss'];
				this.data_url = this.protocol +"://"+ decodedToken['iss']+"/api";
			}
		});

		this.network.onchange().subscribe(data => {
			this.network_status = data.type;
			if(this.network_status === 'offline') {
				this.generic_error_msg = "You are offline. Your request cannot be processed at this time.";
			} else {
				this.generic_error_msg = "Something went wrong. Please try again.";
			}
		});

		this.isApp = (!document.URL.startsWith('http') || document.URL.startsWith('http://localhost:8001'));
	}

	getGuestAccessToken(){
		return new Promise(resolve => {
			let data = {
				appKey: '0o8Rfr1sGyQEt3kXBcXBKh2MsHckSo09x',
			}

			let url = this.data_url+"/api/token";
			let request = this.http.post(url, data).map(res => res.json()).subscribe(data => {
				if(!data['error'] || data['error'] === 0) {
					this.storage.set("sess_user_token", data.token).then((token) => {
						resolve(data);
					});
				}
			}, err => {
				this.showToast(this.generic_error_msg, false);
				resolve({ error: 1, message: this.generic_error_msg });
			});
		});
	}


	getAccessTokenForPost(){
		return new Promise(resolve => {
			if(this.isApp){
				this.getAppVersion();
			}
			this.storage.get("sess_user_token").then((token) => {
				let result = {"token": token};
				resolve(result);
			});
		});
	}

	getVersion() {
		let now = new Date();
		let version = now.getFullYear() +""+ now.getMonth() +""+ now.getDay() +""+ now.getHours() +""+ now.getMinutes() +""+ now.getSeconds() +""+ now.getMilliseconds();
		return version;
	}

	getAppVersion(){
		var today = new Date();
		var h = today.getHours();
		var m = today.getMinutes();

		let current_counter = this.timeToDecimal(h +':'+ m);

		if(current_counter != this.toast_counter){
			this.toast_counter = current_counter;

			this.storage.get("sess_api_version").then((api_version) => {
				console.log('api_version',api_version);
				this.getApiVersion().then(data => {
					if(data['error'] == 0){
						var app_current_version;
						var app_new_version;

					  	if (this.platform.is('ios')) {
					  		app_new_version = data['data']['apple_version'];
						} else if (this.platform.is('android')) {
							app_new_version = data['data']['android_version'];
						}

						this.appVersion.getVersionNumber().then((versionNumber) => {
							console.log('versionNumber',versionNumber);
							if(versionNumber){
								app_current_version = versionNumber;
								this.storage.set("sess_api_version", app_current_version);

								if(api_version){
									if(app_new_version != api_version){
										console.log('not pair');
										data['data']['app_current_version'] = app_current_version;
										data['data']['app_new_version'] = app_new_version;
										this.modalCtrl.create(ModalLocationSettingsPage,{
											'modal_type' : 'api_version',
											'modal_data' : data['data'],
										}).present();
									}
								}
							}
						});
					}
				});
			});
		}else{
			this.toast_counter = current_counter;
		}
	}

	getAccessToken(){
		return new Promise(resolve => {
			if(this.isApp){
				this.getAppVersion();
			}
			this.storage.get("sess_user_token").then((token) => {
				let version = this.getVersion();
				token = token +"&v="+version;

				let result = {"token": token};
				resolve(result);
			});
		});
	}

	getAboutData(){
		return new Promise(resolve => {
			this.getAccessToken().then(data => {
				let url = this.data_url+"/cms?cms=about&token="+ data['token'];
				this.http.get(url).timeout(this.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				}, err => {
					this.showToast(this.generic_error_msg);
					resolve({ error: 1, message: this.generic_error_msg });
				});
			});
		});
	}

	getCmsPageContent(thisPage){
		return new Promise(resolve => {
			this.getAccessTokenForPost().then(data => {
				let url = this.data_url+"/cms";
				url += "?token="+ data['token'];
				url += "&cms="+ thisPage;
				let version = "&v="+ this.getVersion();

				let groupKey = "getCmsPageContent";
				let request = this.http.get(url + version).timeout(this.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.ttl, 'all');

				response.subscribe((data) => {
					resolve(data);
				}, err => {
					this.showToast(this.generic_error_msg);
					resolve({ error: 1, message: this.generic_error_msg });
				});
			});
		});
	}

	getRegions() {
		return new Promise(resolve => {
			this.getAccessTokenForPost().then(data => {
				this.storage.get("sess_regions").then(sess_regions => {
					if(sess_regions) {
						resolve(sess_regions);
					} else {
						let url = this.data_url+"/address/region"
						url += "?token="+ data['token'];
						let version = "&v="+ this.getVersion();
						let groupKey = "getRegions";
						let request = this.http.get(url + version).timeout(this.timeout).map(res => res.json());
						let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.ttl, 'all');
						response.subscribe((data) => {
							this.storage.set("sess_regions", data['provinces']).then((regions) => {
								resolve(data);
							});
						}, err => {
							this.showToast(this.generic_error_msg);
							resolve({ error: 1, message: this.generic_error_msg });
						});
					}
				});
			});
		});
	}

	getCities() {
		return new Promise(resolve => {
			this.storage.get("sess_cities").then((sess_cities) => {
				if(sess_cities) {
					resolve(sess_cities);

				} else {
					this.getAccessTokenForPost().then(data => {
						if(data) {
							let url = this.data_url+"/address/city"
							url += "?token="+ data['token'];
							let version = "&v="+ this.getVersion();
							let groupKey = "getCities";
							let request = this.http.get(url + version).timeout(this.timeout).map(res => res.json());
							let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.ttl, 'all');
							response.subscribe((data) => {
								this.storage.set("sess_cities", data).then((city) => {
									resolve(city);
								});
							}, err => {
								this.showToast(this.generic_error_msg);
								resolve({ error: 1, message: this.generic_error_msg });
							});
						}
					});
				}
			});
		});
	}

	getCitiesByRegions(thisRegion) {
		return new Promise(resolve => {
			this.getAccessTokenForPost().then(data => {
				let url = this.data_url+"/address/city"
				url += "?provCode="+ thisRegion;
				url += "&token="+ data['token'];
				let version = "&v="+ this.getVersion();

				let groupKey = "getCitiesByRegions";
				let request = this.http.get(url + version).timeout(this.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.ttl, 'all');
				response.subscribe((data) => {
					let session_name = "sess_region_city_"+thisRegion;
					this.storage.set(session_name, data).then((city) => {
						resolve(city);
					});
				}, err => {
					this.showToast(this.generic_error_msg);
					resolve({ error: 1, message: this.generic_error_msg });
				});
			});
		});
	}

	getZipcodeByCity(thisCity) {
		return new Promise(resolve => {
			this.getAccessTokenForPost().then(data => {
				let url = this.data_url+"/address/zipcode"
				url += "?cityName="+ thisCity;
				url += "&token="+ data['token'];
				let version = "&v="+ this.getVersion();

				let groupKey = "getZipcodeByCity";
				let request = this.http.get(url + version).timeout(this.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.ttl, 'all');
				response.subscribe((data) => {
					let session_name = "sess_city_zipcode_"+thisCity;
					this.storage.set(session_name, data).then((city) => {
						resolve(city);
					});
				}, err => {
					this.showToast(this.generic_error_msg);
					resolve({ error: 1, message: this.generic_error_msg });
				});
			});
		});
	}

	downloadPhoto(thisPhotos) {
		return new Promise(resolve => {
			thisPhotos.forEach((thisPhoto, idx) => {
				const url = thisPhoto.path;
				const fileTransfer: TransferObject = this.transfer.create();
				fileTransfer.download(encodeURI(url), this.file.externalDataDirectory+thisPhoto.name).then((entry) => {
					console.log('download complete: ' + entry); //
					let pp_pieces = entry.toURL().split("/");
					let fileDownload = {
						"name": pp_pieces[pp_pieces.length - 1],
						"path": entry.toURL(),
					};

					let download_photo = [];
					download_photo.push(fileDownload);

					resolve({"error": 0, "message":"Success Download", "items": download_photo});
				}, (error) => {
					// handle error
				});
			});
		});
	}

	uploadPhoto(thisPhotos) {
		return new Promise(resolve => {
			this.getAccessToken().then(data => {
				let dataLength = (thisPhotos.length - 1);
				let fileUploadStatus = [];
				let url = this.data_url+"/file/mobileupload?token="+ data['token'];

				thisPhotos.forEach((thisPhoto, idx) => {
					let options = {
						fileKey: "file",
						chunkedMode: false,
						mimeType: "image/jpeg",
						fileName: thisPhoto.name,
						headers: {}
					};

					const fileTransfer: TransferObject = this.transfer.create();
					fileTransfer.upload(thisPhoto.path, url, options).then(res => {
						fileUploadStatus[idx] = JSON.parse(res.response);
						if (dataLength === idx) {
							resolve({"error": 0, "message":"Success", "items": fileUploadStatus});
						}
					}, err => {
						resolve({"error": 1, "message":"Cannot upload files"});
						console.log("upload error: ",err);
					});
				});
			});
		});
	}

	saveAttachment(attachmentsData) {
		return new Promise(resolve => {
			this.getAccessTokenForPost().then(data => {
				let url = this.data_url+'/file/save';

				let headers = new Headers();
				headers.append('Content-Type', 'application/x-www-form-urlencoded');
				headers.append('Accept', 'application/json');
				headers.append('Authorization', 'Bearer '+ data['token']);
				let options = new RequestOptions({headers: headers});

				let request = this.http.post(url, attachmentsData, options).timeout(this.timeout).map(res => res.json())
				.subscribe(data1 => { resolve(data1); }, err => {resolve({ error: 1, message: this.generic_error_msg }); });
			});
		});
	}

	getContact(key) {
		return new Promise(resolve => {
			this.getAccessTokenForPost().then(data => {
				let url = this.data_url+'/contactinfo';

				let formData = {
					key: key
				};

				let headers = new Headers();
				headers.append('Content-Type', 'application/x-www-form-urlencoded');
				headers.append('Accept', 'application/json');
				headers.append('Authorization', 'Bearer '+ data['token']);
				let options = new RequestOptions({headers: headers});

				let request = this.http.post(url, formData, options).timeout(this.timeout).map(res => res.json())
				.subscribe(data1 => { resolve(data1); }, err => {resolve({ error: 1, message: this.generic_error_msg }); });
			});
		});
	}

	showToast(thisMsg, display=true) {

		if(!this.network.type || this.network.type == 'none') {
			this.generic_error_msg = "You are offline. Your request cannot be processed at this time.";
		} else {
			this.generic_error_msg = "Something went wrong. Please try again.";
		}

		let options = {
			message: this.generic_error_msg,
			position: "top",
			showCloseButton: true,
			duration: 3000,
			closeButtonText: "x"
		};

		if(display){

			var today = new Date();
			var h = today.getHours();
			var m = today.getMinutes();

			let current_counter = this.timeToDecimal(h +':'+ m);

			if(this.toast_counter){
				if(current_counter != this.toast_counter){
					console.log(current_counter+" "+this.toast_counter);
					this.toastCtrl.create(options).present();
					this.toast_counter = current_counter;
				}

			}else{

				this.toastCtrl.create(options).present();
				this.toast_counter = current_counter;
			}
		}
	}

	timeToDecimal(t) {
		var arr = t.split(':');

		return parseFloat(parseInt(arr[0]) + '.' + parseInt(arr[1]));
	}

	detectCurrentLocation() {
		console.log('fdsfsdssssssssss');
		var options = {
			enableHighAccuracy: true,
			timeout: 5000,
			maximumAge: 0
		};

		this.geolocation.getCurrentPosition(options).then((resp) => {
			console.log('fdsfsdssssssssss',resp);

			if(resp){
				this.coords.lat = resp.coords.latitude;
				this.coords.lng = resp.coords.longitude;

				this.storage.set('sess_user_loc_coords', this.coords).then(() => {
					console.log('sess_user_loc_coords', this.coords);
				});

				console.log('geolocation.getCurrentPosition');
				let options: NativeGeocoderOptions = {
					useLocale: true,
					maxResults: 5
				};

				this.storage.get('sess_user_login').then((user) => {
					if(user && user.address_id){
						let addressData = {
							address_id: user.address_id,
							latitude: this.coords.lat,
							longtitude: this.coords.lng,
						};

						this.updatePatientLocation(addressData).then(res => {
							console.log('addressData', res);
							if(res['error'] == 0){
								user['address_latitude'] = res['data']['latitude'];
								user['address_longtitude'] = res['data']['longtitude'];

								this.storage.set("sess_user_login", user);
							}
						});
					}
				});

				this.nativeGeocoder.reverseGeocode(this.coords.lat, this.coords.lng, options)
					.then((result: NativeGeocoderReverseResult[]) => {
						let loc = result[0].thoroughfare+', '+result[0].locality+', '+result[0].subAdministrativeArea;

						console.log('nativeGeocoder.reverseGeocode');
						if(loc){
							this.storage.set('sess_current_location', loc).then(() => {
								console.log('Item Stored');
							});

							this.storage.get('sess_current_location').then(data => {
								if(data){
									console.log("sess_current_location", data);
									this.events.publish('refresh:mylocation');
								}
							});
						}

					})
					.catch((error: any) => console.log("nativeGeocoderError", error));
			}
		}).catch((error) => {
			console.log('Error getting location', error);
			this.events.publish('cant_get_location:mylocation');
		});

		var today = new Date();
		var h = today.getHours();
		var m = today.getMinutes();

		let current_counter = this.timeToDecimal(h +':'+ m);

		if(this.toast_counter){
			if(current_counter != this.toast_counter){

				this.storage.get('sess_current_location').then(data => {
					if(data){
						this.events.publish('refresh:mylocation');
					}else{
						this.detectCurrentLocation();
					}
				});

				this.toast_counter = current_counter;
			}

		}else{

			this.storage.get('sess_current_location').then(data => {
				if(data){
					this.events.publish('refresh:mylocation');
				}else{
					this.detectCurrentLocation();
				}
			});

			this.toast_counter = current_counter;
		}
	}

	updatePatientLocation(formData) {
		return new Promise(resolve => {
			this.getAccessTokenForPost().then(data => {
				let url = this.data_url+'/user/updatepatientlocation';

				let headers = new Headers();
					// headers.append('Content-Type', 'application/x-www-form-urlencoded');
				headers.append('Accept', 'application/json');
				headers.append('Authorization', 'Bearer '+ data['token']);
				let options = new RequestOptions({headers: headers});

				this.http.post(url, formData, options).timeout(this.timeout).map(res => res.json()).subscribe(data1 => {
					resolve(data1);
				}, err => {
					this.showToast(this.generic_error_msg);
					resolve({ error: 1, message: this.generic_error_msg });
				});
			});
		});
	}

	getApiVersion() {
		return new Promise(resolve => {
			this.getAccessToken().then(data => {
				let url = this.data_url+"/api/checkversion?";
				url += "token="+ data['token'];
				url += "&type=patient_app";
				this.http.get(url).timeout(this.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				}, err => {
					this.showToast(this.generic_error_msg);
					resolve({ error: 1, message: this.generic_error_msg });
				});
			});
		});
	}
}
