import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';
import {JwtHelper} from "angular2-jwt";
/*
	Generated class for the HospitalsProvider provider.

	See https://angular.io/guide/dependency-injection for more info on providers
	and Angular DI.
*/
@Injectable()
export class HospitalsProvider {

	jwtHelper = new JwtHelper();
	public base_url: any;
	data_url: any;

	constructor(public http: Http, 
				private cache: CacheService, 
				public storage: Storage, 
				private siteProvider: SiteProvider) {

		console.log('Hello HospitalsProvider Provider');
		this.storage.get("sess_user_token").then((token) => {
			// Decode the Token
			let decodedToken = this.jwtHelper.decodeToken(token);
			this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
			this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
		});
	}

	getAllHospitalDoctors(isLoadNew, thisData){
		return new Promise(resolve => {
			this.siteProvider.getAccessToken().then(data => {
				let url = this.data_url+"/hospital/doctors?id="+ thisData.id;
				url += "&token="+ data['token'];
				this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				}, err => { 
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg }); 
				});
			});
		});
	}

	getServices(thisHospital){
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				let url = this.data_url+"/hospital/service?id="+ thisHospital;
				url += "&token="+ data['token'];
				let version = "&v="+ this.siteProvider.getVersion();

				let groupKey = "getServices";
				let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
				response.subscribe((data) => {
					resolve(data);
				}, err => { 
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg }); 
				});
			});
		});
	}

	getAllHospitals(isLoadNew, thisData){
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				let url = this.data_url+"/search/result?action=hospitals&q="+ thisData.searched_keyword;
				if(thisData.applied_filters && thisData.applied_filters.city) {
					url += "&city="+ thisData.applied_filters.city;
				}
				 if(thisData.origin_coordinates) {
					url += "&lat="+ thisData.origin_coordinates.lat;
					url += "&lng="+ thisData.origin_coordinates.lng;
				}
				url += "&page="+ thisData.current_page;
				url += "&orderby=name&orderdirection=asc";
				url += "&token="+ data['token'];
				let version = "&v="+ this.siteProvider.getVersion();
				let groupKey = "getAllHospitals";

				let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
				response.subscribe((data) => {
					resolve(data);
				}, err => { 
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg }); 
				});
			});
		});
	}
}
