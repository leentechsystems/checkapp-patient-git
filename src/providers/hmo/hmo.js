import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';
import { JwtHelper } from "angular2-jwt";
/*
    Generated class for the HmoProvider provider.

    See https://angular.io/guide/dependency-injection for more info on providers
    and Angular DI.
*/
var HmoProvider = /** @class */ (function () {
    function HmoProvider(http, cache, siteProvider, storage) {
        var _this = this;
        this.http = http;
        this.cache = cache;
        this.siteProvider = siteProvider;
        this.storage = storage;
        this.jwtHelper = new JwtHelper();
        console.log('Hello HmoProvider Provider');
        this.storage.get("sess_user_token").then(function (token) {
            // Decode the Token
            var decodedToken = _this.jwtHelper.decodeToken(token);
            _this.base_url = _this.siteProvider.protocol + "://" + decodedToken['iss'];
            _this.data_url = _this.siteProvider.protocol + "://" + decodedToken['iss'] + "/api";
        });
    }
    HmoProvider.prototype.getUserHmoAccount = function (loadNew, thisData) {
        var _this = this;
        console.log(thisData);
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + '/hmo';
                var thisdata = JSON.stringify({
                    "userId": parseInt(thisData.id)
                });
                var headers = new Headers();
                headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + data['token']);
                var options = new RequestOptions({ headers: headers });
                _this.http.post(url, thisdata, options).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.applyNewAccount = function (thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + '/hmo/newaccount';
                var headers = new Headers();
                headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + data['token']);
                var options = new RequestOptions({ headers: headers });
                _this.http.post(url, thisData, options).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.getAllTodayTransactions = function (isLoadNew, thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessToken().then(function (data) {
                var url = _this.data_url + "/hmo/transactions?";
                url += "page=" + thisData.current_page;
                url += "&token=" + data['token'];
                _this.http.get(url).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.getAllPreviousTransactions = function (isLoadNew, thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + "/hmo/transactions?";
                url += "page=" + thisData.current_page;
                url += "&token=" + data['token'];
                var version = "&v=" + _this.siteProvider.getVersion();
                var groupKey = "getAllPreviousTransactions";
                var request = _this.http.get(url + version).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); });
                var response = _this.cache.loadFromDelayedObservable(url, request, groupKey, _this.siteProvider.ttl, 'all');
                response.subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.getUserHmoTransactionsDeductions = function (thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessToken().then(function (data) {
                var url = _this.data_url + "/hmo/billing?";
                url += "billingstart=" + thisData.billingstart;
                url += "&billingend=" + thisData.billingend;
                url += "&token=" + data['token'];
                _this.http.get(url).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.uploadPaymentSlip = function (thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + '/payment/uploadpayment';
                var thisdata = JSON.stringify(thisData);
                var headers = new Headers();
                headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + data['token']);
                var options = new RequestOptions({ headers: headers });
                _this.http.post(url, thisdata, options).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.getPaymentHistoryTransactions = function (isLoadNew, thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + "/hmo/payment?";
                url += "hmo_account_id=" + thisData.hmo_account_id;
                url += "&page=" + thisData.current_page;
                url += "&token=" + data['token'];
                var version = "&v=" + _this.siteProvider.getVersion();
                var groupKey = "getAllPreviousPaymentTransactions";
                var request = _this.http.get(url + version).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); });
                var response = _this.cache.loadFromDelayedObservable(url, request, groupKey, _this.siteProvider.ttl, 'all');
                response.subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.getPaymentHistoryById = function (thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + "/hmo/payment?";
                url += "id=" + thisData.payment_history_id;
                url += "&token=" + data['token'];
                var version = "&v=" + _this.siteProvider.getVersion();
                var groupKey = "getPaymentHistoryById=" + thisData.payment_history_id;
                var request = _this.http.get(url + version).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); });
                var response = _this.cache.loadFromDelayedObservable(url, request, groupKey, _this.siteProvider.ttl, 'all');
                response.subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.getPaymentGateway = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessToken().then(function (data) {
                var url = _this.data_url + "/payment/paymentgateway?";
                url += "token=" + data['token'];
                _this.http.get(url).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.getHmoProvider = function (thisData) {
        var _this = this;
        console.log(thisData);
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessToken().then(function (data) {
                var url = _this.data_url + "/hmoprovider?";
                url += "id=" + thisData['id'];
                url += "&token=" + data['token'];
                _this.http.get(url).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    HmoProvider.ctorParameters = function () { return [
        { type: Http, },
        { type: CacheService, },
        { type: SiteProvider, },
        { type: Storage, },
    ]; };
    return HmoProvider;
}());
export { HmoProvider };
//# sourceMappingURL=hmo.js.map
