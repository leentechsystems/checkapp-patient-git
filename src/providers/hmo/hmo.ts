import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';
import {JwtHelper} from "angular2-jwt";

/*
	Generated class for the HmoProvider provider.

	See https://angular.io/guide/dependency-injection for more info on providers
	and Angular DI.
*/
@Injectable()
export class HmoProvider {

	jwtHelper = new JwtHelper();
	public base_url: any;
	data_url: any;

	constructor(public http: Http,
				private cache: CacheService,
				public siteProvider: SiteProvider,
				public storage: Storage) {

		console.log('Hello HmoProvider Provider');
		this.storage.get("sess_user_token").then((token) => {
		 	// Decode the Token
			let decodedToken = this.jwtHelper.decodeToken(token);
			this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
			this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
		});
	}

	getUserHmoAccount(loadNew, thisData) {
		console.log(thisData);
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				var url = this.data_url+'/hmo';

				var thisdata = JSON.stringify({
					"userId": parseInt(thisData.id)
				});

				let headers = new Headers();
				headers.append('Content-Type', 'application/x-www-form-urlencoded');
				headers.append('Accept', 'application/json');
				headers.append('Authorization', 'Bearer '+ data['token']);
				let options = new RequestOptions({headers: headers});

				this.http.post(url, thisdata, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	applyNewAccount(thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				var url = this.data_url+'/hmo/newaccount';

				let headers = new Headers();
				headers.append('Content-Type', 'application/x-www-form-urlencoded');
				headers.append('Accept', 'application/json');
				headers.append('Authorization', 'Bearer '+ data['token']);
				let options = new RequestOptions({headers: headers});

				this.http.post(url, thisData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	getAllTodayTransactions(isLoadNew, thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessToken().then(data => {
				let url = this.data_url+"/hmo/transactions?";
				url += "page="+ thisData.current_page;
				url += "&token="+ data['token'];
				this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}


	getAllPreviousTransactions(isLoadNew, thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				let url = this.data_url+"/hmo/transactions?";
				url += "page="+ thisData.current_page;
				url += "&token="+ data['token'];
				let version = "&v="+ this.siteProvider.getVersion();

				let groupKey = "getAllPreviousTransactions";
				let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

				response.subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	getUserHmoTransactionsDeductions(thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessToken().then(data => {
				let url = this.data_url+"/hmo/billing?";
				url += "billingstart="+ thisData.billingstart;
				url += "&billingend="+ thisData.billingend;
				url += "&token="+ data['token'];
			 	this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				 }, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	uploadPaymentSlip(thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				var url = this.data_url+'/payment/uploadpayment';
				var thisdata = JSON.stringify(thisData);
				let headers = new Headers();
				headers.append('Content-Type', 'application/x-www-form-urlencoded');
				headers.append('Accept', 'application/json');
				headers.append('Authorization', 'Bearer '+ data['token']);
				let options = new RequestOptions({headers: headers});
				this.http.post(url, thisdata, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	getPaymentHistoryTransactions(isLoadNew, thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				let url = this.data_url+"/hmo/payment?";
				url += "hmo_account_id="+ thisData.hmo_account_id;
				url += "&page="+ thisData.current_page;
				url += "&token="+ data['token'];
				let version = "&v="+ this.siteProvider.getVersion();

				let groupKey = "getAllPreviousPaymentTransactions";
				let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
			 	let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

				response.subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	getPaymentHistoryById(thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				let url = this.data_url+"/hmo/payment?";
				url += "id="+ thisData.payment_history_id;
				url += "&token="+ data['token'];
				let version = "&v="+ this.siteProvider.getVersion();

				let groupKey = "getPaymentHistoryById="+thisData.payment_history_id;
				let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

				response.subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	getPaymentGateway() {
		return new Promise(resolve => {
			this.siteProvider.getAccessToken().then(data => {
				let url = this.data_url+"/payment/paymentgateway?";
				url += "token="+ data['token'];
				this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	getHmoProvider(thisData) {
		console.log(thisData);
		return new Promise(resolve => {
			this.siteProvider.getAccessToken().then(data => {
				let url = this.data_url+"/hmoprovider?";
				url += "id="+ thisData['id'];
				url += "&token="+ data['token'];
				this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}
}
