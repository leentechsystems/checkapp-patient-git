import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';
import {JwtHelper} from "angular2-jwt";

/*
	Generated class for the DoctorProvider provider.

	See https://angular.io/guide/dependency-injection for more info on providers
	and Angular DI.
*/
@Injectable()
export class DoctorProvider {

	jwtHelper = new JwtHelper();
	public base_url: any;
	data_url: any;

	accessToken:any = "";

	constructor(public http: Http,
				private cache: CacheService,
				private siteProvider: SiteProvider,
				public storage: Storage) {

		console.log('Hello DoctorProvider Provider');
		this.storage.get("sess_user_token").then((token) => {
			// Decode the Token
			let decodedToken = this.jwtHelper.decodeToken(token);
			this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
			this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
		});
	}

	getSpecializations(isLoadNew, thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				this.accessToken = data['token'];
				let url = this.data_url+"/search/doctorspecialties?q="+ thisData.searched_keyword;
				url += "&page="+ thisData.current_page;
				url += "&token="+ data['token'];
				let version = "&v="+ this.siteProvider.getVersion();

				let groupKey = "getSpecializations";
				let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

				response.subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	getAllDoctors(isLoadNew, thisData){
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {
				this.accessToken = data['token'];
				let url = this.data_url+"/search/result?action=doctors";

				if(thisData.applied_filters && thisData.applied_filters.city) {
					url += "&city="+ thisData.applied_filters.city;
				}

				if(thisData.origin_coordinates) {
					url += "&lat="+ thisData.origin_coordinates.lat;
					url += "&lng="+ thisData.origin_coordinates.lng;
				}

				if(thisData.hospital) {
				 	url += "&hospital="+ thisData.hospital;
				}

				url += "&q="+ thisData.searched_keyword;
				url += "&specialty="+ thisData.selected_specialties;
				url += "&page="+ thisData.current_page;
				url += "&orderby=firstname&orderdirection=asc";
				url += "&token="+ data['token'];
				let version = "&v="+ this.siteProvider.getVersion();

				let groupKey = "getAllDoctors";
				let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

				response.subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	getSchedules(isLoadNew, thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessToken().then(data => {
				this.accessToken = data['token'];
				let url = this.data_url+"/user/doctortimeslot?";
				url += "id="+thisData.doc_id;
				url += "&hospital="+ thisData.hospital_id;
				url += "&day="+ thisData.selected_day;
				url += "&token="+ data['token'];
				let request = this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json());

				request.subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}

	getUserServices(thisData) {
		return new Promise(resolve => {
			this.siteProvider.getAccessTokenForPost().then(data => {

				let url = this.data_url+"/user/doctorservices";
				url += "?token="+ data['token'];
				url += "&hospital_id="+ thisData.hospital_id;
				url += "&doctor_id="+ thisData.doctor_id;

				if(thisData.service_type_id){
					url += "&service_type_id="+ thisData.service_type_id;
				}

				let version = "&v="+ this.siteProvider.getVersion();

				let groupKey = "getUserServices";
				let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
				let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

				request.subscribe((data) => {
					resolve(data);
				}, err => {
					this.siteProvider.showToast(this.siteProvider.generic_error_msg);
					resolve({ error: 1, message: this.siteProvider.generic_error_msg });
				});
			});
		});
	}
}
