import { Component } from '@angular/core';
import { Platform, ModalController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { Storage } from '@ionic/storage';
import { CacheService } from "ionic-cache";
import { Network } from '@ionic-native/network';
import { ImageLoaderConfig } from 'ionic-image-loader';
import { Badge } from '@ionic-native/badge';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  public google_api_key = "AIzaSyBs-lDYIShK6wLxjKXKf8nqYhxaDVbdM-E";
  toast_counter = 0;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private storage: Storage, 
   public cache: CacheService, public modalCtrl: ModalController, public network: Network, public toastCtrl: ToastController,
   private imageLoaderConfig: ImageLoaderConfig, public badge: Badge) {

    this.imageLoaderConfig.setBackgroundSize('cover');
    this.imageLoaderConfig.setCacheDirectoryName('checkapp-image-cache');
    this.imageLoaderConfig.setImageReturnType('base64');
    this.imageLoaderConfig.setFallbackFileNameCachedExtension('.jpg');
    this.imageLoaderConfig.enableSpinner(false);
    this.imageLoaderConfig.setFallbackUrl('assets/imgs/placeholder.png');
    this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
    this.imageLoaderConfig.setMaximumCacheAge(7 * 24 * 60 * 60 * 1000); /*7 days*/
    this.imageLoaderConfig.enableDebugMode();
    this.imageLoaderConfig.setHeight('auto');

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      
      this.cache.setDefaultTTL(60 * 60 * 3); //set default cache TTL for 1 hour
      this.cache.setOfflineInvalidate(false); // Keep our cached results when device is offline!

      storage.ready().then(() => {
        storage.set('sess_google_api_key', this.google_api_key).then(key => { });

        storage.get('sess_user_login').then((val) => {

          this.network.onchange().subscribe(data => {
            console.log('data network', data);
            let options = {};

            if(data.type === "offline") {
              options = {
                  message: "You are offline",
                  position: "top",
                  showCloseButton: true,
                  duration: 5000,
                  closeButtonText: "x"
              };
            }  else {
              options = {
                  message: "You are now connected",
                  position: "top",
                  duration: 5000,
                  cssClass: "success"
              };
            }

            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();

            let current_counter = this.timeToDecimal(h +':'+ m);

            if(this.toast_counter){
              if(current_counter != this.toast_counter){
                console.log(current_counter+" "+this.toast_counter);
                this.toastCtrl.create(options).present();
                this.toast_counter = current_counter;
              }

            }else{

              this.toastCtrl.create(options).present();
              this.toast_counter = current_counter;
            }
          });

          if(!val) {
            this.rootPage = WalkthroughPage;
            this.badge.clear();

          } else {
            this.rootPage = TabsPage;
          }
        });

        /*statusBar.styleDefault();*/
        statusBar.styleLightContent();
        statusBar.backgroundColorByHexString("#0aaddd");
        splashScreen.hide();
        
      });

      /*statusBar.styleDefault();*/
      statusBar.styleLightContent();
      statusBar.backgroundColorByHexString("#0aaddd");
      splashScreen.hide();

    });
  }
  
  timeToDecimal(t) {
    var arr = t.split(':');

    return parseFloat(parseInt(arr[0]) + '.' + parseInt(arr[1]));
  }  
}


/*
if (platform.is('ios')) {
  
} else if (platform.is('android')) {

}
*/